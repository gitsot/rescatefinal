var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var SEED = require('../config/config').SEED;
var Usuario = require('../models/usuario');

/**
 * LOGIN PARA EL PACIENTE
 */
function logUsuario(req, res, next){
    var body = req.body;
    Usuario.findOne( {dni: body.dni}, (err, usuarioDB)=>{

        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar usuarios',
                errors: err
            });
        }

        if (!usuarioDB){
            return res.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrectas',
                errors: err
            });
        }

        if( !bcrypt.compareSync(body.password, usuarioDB.password)){
            return res.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrectas',
                errors: err
            });
        }

        usuarioDB.password = 'Me gusta la pizza con piña';
        var token = jwt.sign({ paciente: usuarioDB }, SEED,{expiresIn: 14400 }); //4 el tokan expira en cuatro horas

        res.status(200).json({
            ok: true,
            json: usuarioDB,
            token: token,
            id: usuarioDB._id
        });
    });
}
module.exports = {
    logUsuario
};