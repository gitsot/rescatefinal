var express = require('express');
var app = express();
var usuarioController = require('../controllers/usuarioController.js');
//var mdAutenticacion = require('../middlewares/autenticacion'); // Al usar esta variable verifica el token

app.get('/', /*mdAutenticacion.verificaToken,*/ usuarioController.getUsuarios); // Visualiza TODOS los usuarios
app.get('/:id', /*mdAutenticacion.verificaToken,*/ usuarioController.getUsuario); // Visualiza UN usuario
app.post('/', /*mdAutenticacion.verificaToken,*/ usuarioController.postUsuario); // Crea un usuario
app.post('/:idUsuario', /*mdAutenticacion.verificaToken,*/ usuarioController.putUsuario); // Modifica un usuario
app.post('/borrar/:id', /*mdAutenticacion.verificaToken,*/ usuarioController.deleteUsuario); // Borra un usuario
app.post('/:idUsuario/:idGrupo', /*mdAutenticacion.verificaToken,*/ usuarioController.putAsignaGrupoUsuario); // Asigna un grupo a un usuario

module.exports = app;