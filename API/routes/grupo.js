var express = require('express');
var app = express();
var grupoController = require('../controllers/grupoController.js');
//var mdAutenticacion = require('../middlewares/autenticacion'); // Al usar esta variable verifica el token


app.get('/listaBusquedaCoordenadas/:idBusqueda', /*mdAutenticacion.verificaToken,*/ grupoController.getGruposBusquedaCoordenadas); // Visualiza todas las coordenadas de grupos en una busqueda
app.get('/listaBusqueda/:idBusqueda', /*mdAutenticacion.verificaToken,*/ grupoController.getGruposBusqueda); // Visualiza todos los grupos de una busqueda
app.get('/listaUsuario/:idUsuario', /*mdAutenticacion.verificaToken,*/ grupoController.getGruposUsuario); // Visualiza todos los grupos de un usuario
app.get('/:id', /*mdAutenticacion.verificaToken,*/ grupoController.getGrupo); // Muestra los datos de un grupo
app.get('/usuarios/:id', /*mdAutenticacion.verificaToken,*/ grupoController.getUsuarios); // Muestra todos los usuarios de un grupo
/*NO LA USO PARA NADA (POR AHORA)*/app.get('/perteneceBusqueda/:idBusqueda/:idUsuario', /*mdAutenticacion.verificaToken,*/ grupoController.perteneceBusqueda); // Devuelve un boolean si ese usuario pertenece a una búsqueda
app.get('/meteGrupo/:idBusqueda/:idUsuario', /*mdAutenticacion.verificaToken,*/ grupoController.meteGrupo); // Mete a un usuario en un grupo y lo devuelve
app.post('/', /*mdAutenticacion.verificaToken,*/ grupoController.postGrupo); // Crea un grupo
app.post('/:id', /*mdAutenticacion.verificaToken,*/ grupoController.putGrupo); // Modifica un grupo
app.post('/borrar/:id', /*mdAutenticacion.verificaToken,*/ grupoController.deleteGrupo); // Borra un grupo
app.post('/borrar/:idUsuario/:idGrupo', /*mdAutenticacion.verificaToken,*/ grupoController.deleteRelationGrupo); // Borra la relación del usuario y el grupo


//app.delete('/:id', /*mdAutenticacion.verificaToken,*/ busquedaController.deleteConsultasUsusario); // no creo que borre búsquedas

module.exports = app;