var mongoose = require('mongoose');
//var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var grupoSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es obligatorio'] },
    coordenadas: { type: [JSON], required: [true, 'Las coordenadas son obligatorias'] },
    fechaCreado: { type: Date, required: [true, 'La fecha es obligatoria'] },
    id_busqueda:{ type:Schema.Types.ObjectId, ref: 'busqueda' },
    id_usuario:{ type:[Schema.Types.ObjectId], ref: 'usuario' },
});

//pacienteSchema.plugin( uniqueValidator, { message: 'el {PATH} debe ser único' });
module.exports = mongoose.model('grupo', grupoSchema);

