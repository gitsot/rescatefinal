/**
 * Carga la búsqueda elegida y la ruta para modificar esa búsqueda
 * @param selectObject
 * @returns {Promise<void>}
 */
async function cargarBusquedaModificar(selectObject) {
    // Recojo de la API los datos de la búsqueda y la pinto en los texbox
    let res = await axios.get('http://localhost:3000/busqueda/'+selectObject.value);
    var data = JSON.parse(JSON.stringify(res.data)).json;

    document.getElementById("nombre").value = data.nombre;
    document.getElementById("descripcion").value = data.descripcion;
    document.getElementById("nombreZona").value = data.nombreZona;
    document.getElementById("esActiva").checked = data.esActiva;
    document.getElementById("frmCoordenadas").value = JSON.stringify(data.coordenadas);

    // pongo el botón en verde para indicar que hay una coordenada guardada
    document.getElementById("ardid").className = "btn btn-large btn-positive";
    // Modifico la ruta para elegir el id correcto
    document.getElementById('form').action = "http://localhost:3000/busqueda/"+data._id;
}

/**
 * Carga la búsqueda elegida y la ruta para borrar esa búsqueda
 * @param selectObject
 * @returns {Promise<void>}
 */
async function cargarBusquedaBorrar(selectObject) {
    // Recojo de la API los datos de la búsqueda y la pinto en los texbox
    let res = await axios.get('http://localhost:3000/busqueda/'+selectObject.value);
    var data = JSON.parse(JSON.stringify(res.data)).json;

    document.getElementById("nombre").innerHTML = data.nombre;
    document.getElementById("descripcion").innerHTML = data.descripcion;
    document.getElementById("nombreZona").innerHTML = data.nombreZona;
    document.getElementById("fechaCreado").innerHTML = data.fechaCreado;
    document.getElementById("esActiva").innerHTML = data.esActiva === true ? "Activa" : "Inactiva";

    // Modifico la ruta para elegir el id correcto
    document.getElementById('form').action = "http://localhost:3000/busqueda/borrar/"+data._id+"?_method=PUT";
}

/**
 * Esta función vacía los labels al hacer submit
 */
function labelBlankBusqueda(){
    // Elinimo del desplegable la opción que acabo de eliminar
    var x = document.getElementById("busq");
    x.remove(x.selectedIndex);

    // Vuelvo a poner en blando todos los valores
    document.getElementById("nombre").innerHTML = "";
    document.getElementById("descripcion").innerHTML = "";
    document.getElementById("nombreZona").innerHTML = "";
    document.getElementById("fechaCreado").innerHTML = "";
    document.getElementById("esActiva").innerHTML = "";
}

/**
 * Modifica el nombre del desplegable cuando se envía el formulario
 */
function setOption(){
    var p = document.getElementById('busq')
    p.options[p.selectedIndex].text = document.getElementById('form').nombre.value

    // pongo el botón en rojo para indicar que no hay coordenadas guardadas
    document.getElementById("ardid").className = "btn btn-large btn-negative";
}