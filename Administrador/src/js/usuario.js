/**
 * Carga el usuario elegido para modificarlo
 * @param selectObject
 * @returns {Promise<void>}
 */
async function cargarUsuarioModificar(selectObject) {
    // Recojo de la API los datos del usuario y la pinto en los texbox
    let res = await axios.get('http://localhost:3000/usuario/'+selectObject.value);
    var data = JSON.parse(JSON.stringify(res.data)).json;

    document.getElementById("nombre").value = data.nombre;
    document.getElementById("apellido").value = data.apellido;
    document.getElementById("dni").value = data.dni;
    document.getElementById("esAdmin").checked = data.esAdmin;

    // Modifico la ruta para elegir el id correcto
    document.getElementById('form').action = "http://localhost:3000/usuario/"+data._id;
}

/**
 * Modifica el nombre del desplegable cuando se envía el formulario
 */
function setOption(){
    var p = document.getElementById('usu')
    p.options[p.selectedIndex].text = document.getElementById('form').nombre.value
}

/**
 * Carga el grupo elegido y la ruta para borrar ese grupo
 * @param selectObject
 * @returns {Promise<void>}
 */
async function cargarUsuarioBorrar(selectObject) {
    // Recojo de la API los datos del usuario y la pinto en los texbox
    let res = await axios.get('http://localhost:3000/usuario/'+selectObject.value);
    var data = JSON.parse(JSON.stringify(res.data)).json

    // Recojo de la API los datos de los grupos a los que pertenece el usuario y la pinto en los texbox
    let resGru = await axios.get('http://localhost:3000/grupo/listaUsuario/'+selectObject.value);
    var dataGru = JSON.parse(JSON.stringify(resGru.data)).json;

    document.getElementById("nombre").innerHTML = data.nombre;
    document.getElementById("apellido").innerHTML = data.apellido;
    document.getElementById("dni").innerHTML = data.dni;
    document.getElementById("nombre").innerHTML = data.nombre;
    document.getElementById("esAdmin").innerHTML = data.esAdmin === true ? "Es administrador" : "No es administrador";

    dataGru.forEach( function(valor, indice, array) {
        var node = document.createElement("P");
        var textnode = document.createTextNode(valor.nombre);
        node.appendChild(textnode);
        document.getElementById("grupo").appendChild(node);
    });

    // Modifico la ruta para elegir el id correcto
    document.getElementById('form').action = "http://localhost:3000/usuario/borrar/"+data._id;
}

/**
 * Pongo en blanco los labels al hacer submit y borro del desplebagle la opción que he eliminado
 */
function labelBlankUsuario(){
    // Elimino del desplegable la opción que acabo de eliminar
    var x = document.getElementById("usu");
    x.remove(x.selectedIndex);

    // Vuelvo a poner en blanco todos los valores
    document.getElementById("nombre").innerHTML = "";
    document.getElementById("apellido").innerHTML = "";
    document.getElementById("dni").innerHTML = "";
    document.getElementById("esAdmin").innerHTML = "";
}

/**
 * Función para modificar la ruta para modificar a un usuario
 * @param url
 */
function cargarUsuarioAsignar(url) {
    // Modifico la ruta para elegir el id del usuario y el grupo a asignar
    document.getElementById('form').action = "http://localhost:3000/usuario/"+url;+"?_method=PUT";
}

/**
 * Vacía el desplegable de grupo en asignar
 */
function setOptionAsignar(){
    var x = document.getElementById("grup");
    for (var i = 0; i < x.length+1; i++) {
        x.remove(1);
    }
}

/**
 * Función para modificar la ruta para borrar la relación de un usuario con un grupo
 * @param url
 */
function cargarUrlBorrarRelacion(url) {
    // Modifico la ruta para borrar la relacion con los ids
    document.getElementById('form').action = "http://localhost:3000/grupo/borrar/"+url;+"?_method=PUT";
}