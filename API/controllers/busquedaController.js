var Busqueda = require('../models/busqueda');
var Grupo = require('../models/grupo');
var Mensaje = require('../models/mensajes');
var Coordenadas = require('../models/coordenadas');
var grupoController = require('./grupoController');
var mensajeController = require('./mensajeController');
var coordenadaController = require('./coordenadaController');
var mongoose = require('mongoose');

/**
 * GET BUSQUEDAS ACTIVAS
 * Devuelve los nombres de las busquedas activas
 */
function getBusquedas(req, res, next){
    // Si quiero que se muestren solo las activas o inactivas mando parámetro a true o false, y en el $or de la consulta se buscan,
    // si quiero que se muestren TODAS las busquedas en la consulta busco con true or false
    // Esto lo hago para no repetir código y hacer otra consulta para las busquedas activas e inactivas
    var p1,p2;
    if(req.query.activas!=undefined && (req.query.activas!=undefined==true || req.query.activas!=undefined==false)){
        p1=req.query.activas;
        p2=req.query.activas;
        console.log(req.query.activas);
    }else{
        p1=true;
        p2=false;
    }

    Busqueda.find({ $or:[{esActiva: p1},{esActiva: p2}] }).sort({fechaCreado: -1})
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    json
                });
            });
}

/**
 * Devuelve los datos de una busqueda por id
 * @param req
 * @param res
 * @param next
 */
function getBusqueda(req, res, next){
    Busqueda.findOne({ _id: req.params.id })
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    json
                });
            });
}



/**
* POST BUSQUEDA
* Crea una busqueda
*/
function postBusqueda(req, res, next){
    var body = req.body;

    // Notifico al servidor de que se ha creado una nueva búsqueda, el servidor de sockets se encargará de mandar el mensaje a los clientes
    const io = require('socket.io-client');
    var notificacion = io.connect('http://localhost:4000');
    notificacion.emit("notificacionInterna", "nuevaBusqueda");

    var busqueda = new Busqueda({
        nombre: body.nombre,
        descripcion: body.descripcion,
        nombreZona: body.nombreZona,
        fechaCreado: new Date(),
        coordenadas: JSON.parse(body.coordenadas),
        esActiva: true,
    });

    busqueda.save( ( err, busquedaGuardada ) => {
        if (err){
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear la busqueda',
                errors: err
            });
        }
        /*res.status(201).json({
            ok: true,
            busqueda: busquedaGuardada
        });*/
    });
}



/**
 * PUT BUSQUEDA
 * Actualiza una búsqueda con un id
 */
function putBusqueda(req, res, next){
    var id = req.params.id;
    var body = req.body;

    Busqueda.findById(id, (err, busqueda)=>{
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar la busqueda',
                errors: err
            });
        }

        if (!busqueda){
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Esa busqueda no existe',
                    errors: err
                });
            }
        }

        busqueda.nombre = body.nombre;
        busqueda.descripcion = body.descripcion;
        busqueda.nombreZona = body.nombreZona;

        if(body.coordenadas!=='undefined') {
            busqueda.coordenadas = JSON.parse(body.coordenadas);
        }

        if(body.esActiva == 'on'){ // Si el estado de la busqueda no viene desde el formulario quiere decir que el checkbox no se ha seleccionado, así que será false (no activa)
            busqueda.esActiva = true
        }else{
            busqueda.esActiva = false
        }

        busqueda.fechaCreado = busqueda.fechaCreado // Mantengo la fecha de creado porque no se puede modificar

        busqueda.save( ( err, busquedaGuardada ) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar el usuario',
                    errors: err
                });
            }

            /*res.status(200).json({
                ok: true,
                paciente: busquedaGuardada
            });*/
        });
    });
}

/**
 * DELETE BUSQUEDA
 * Borra una búsqueda
 * @param req
 * @param res
 * @param next
 */
function deleteBusqueda(req, res, next){
    var id = req.params.id;

    Busqueda.findByIdAndRemove(id, (err, BusquedaBorrada)=>{
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar la búsqueda',
                errors: err
            });
        }

        if (!BusquedaBorrada){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe esa búsqueda con ese id',
            });
        }

        // Borro TODOS los grupos que pertenecen a esa búsqueda
        Grupo.find({ id_busqueda : id }).exec((err, json) => {
            json.forEach( function(valor, indice, array) {
                req.params.id = valor._id
                grupoController.deleteGrupo(req, null, null)


                // Borro TODOS los mensajes que pertenecen a ese grupo
                Mensaje.find({ id_grupo : valor._id }).exec((err, json) => {
                    json.forEach( function(valor, indice, array) {
                        req.params.id = valor._id
                        mensajeController.deleteMensaje(req, null, null)
                    });
                });

                // Borro TODAS las coordenadas que pertenecen a ese grupo
                Coordenadas.find({ id_Grupo : valor._id }).exec((err, json) => {
                    json.forEach( function(valor, indice, array) {
                        req.params.id = valor._id
                        coordenadaController.deleteCoordenadas(req, null, null)
                    });
                });
            });
        });


        /*res.status(200).json({
            ok: true,
            consulta: BusquedaBorrada
        });*/
    });
}

function getHorasBusqueda(req, res, next){
    Busqueda.aggregate([
        {
            $match: { "_id" : new mongoose.Types.ObjectId(req.params.idBusqueda)}

        },
        {
            $project: {
                horasAcumuladas: {
                    $subtract: [ new Date(), "$fechaCreado" ]
                }
            }
        }
    ]).exec(
        (err, json) => {
            if (err){
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error de base de datos',
                    errors: err
                });
            }


            res.status(200).json({
                ok: true,
                json
            });
        });
}

function getnParticipantesDeGrupo(req, res, next){
    Grupo.aggregate([
        {
            $match: { "id_busqueda" : new mongoose.Types.ObjectId(req.params.idBusqueda)}

        },

        {
            $project:{
                nParticipantes: { $size: "$id_usuario" }
            }
        }
    ]).exec(
        (err, json) => {
            if (err){
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error de base de datos',
                    errors: err
                });
            }


            res.status(200).json({
                ok: true,
                json
            });
        });
}

module.exports = {
    postBusqueda,
    getBusquedas,
    putBusqueda,
    getBusqueda,
    deleteBusqueda,
    getHorasBusqueda,
    getnParticipantesDeGrupo
};