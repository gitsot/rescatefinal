const io = require('socket.io-client');
var chat = io.connect('http://localhost:4000');

/**
 * Obtiene una lista de grupos
 */
getGrupos();
async function getGrupos() {
    // Recojo de la API los datos de la búsqueda y la pinto en los texbox
    let res = await axios.get('http://localhost:3000/grupo/listaBusqueda/'+localStorage.getItem('idBusqueda'));
    var data = JSON.parse(JSON.stringify(res.data)).json;

    data.forEach( function(valor, indice, array) {
        // Creo la lista de grupos
        var li = document.createElement("li");
        li.classList.add("list-group-header"); // Le doy la clase del li
        li.classList.add("opcion"); // Le doy la clase del li
        li.onclick = function() { abrirChat(valor._id); };
        var img = document.createElement("img");
        img.classList.add("limg-circle"); // Le doy la clase del img
        img.classList.add("media-object"); // Le doy la clase del img
        img.classList.add("pull-left"); // Le doy la clase del img
        img.src = "./src/resources/locard.png";

        var divv = document.createElement("div");
        divv.classList.add("media-body"); // Le doy la clase del div

        var strong = document.createElement("strong");
        var sData = document.createTextNode(valor.nombre);
        var par = document.createElement("p");
        var pData = document.createTextNode("Fecha Creado: " + obtenerFecha(valor.fechaCreado));
        li.appendChild(img);
        li.appendChild(divv);
        divv.appendChild(strong);
        strong.appendChild(sData);
        divv.appendChild(par);
        par.appendChild(pData);

        var element = document.getElementById("gruposBusqueda");
        element.appendChild(li);
    });
}

/**
 * Pequeña función para obtener la fecha adaptada a un string
 * @param fecha
 * @returns {string}
 */
function obtenerFecha(fecha){
    var date = new Date(fecha);
    date.setMonth(date.getMonth()+1);

    var fechaFinal="";
    fechaFinal+=date.getDate() + "/"; // Añado el dia
    fechaFinal+=(date.getMonth()+1 < 10 ? '0' : '') + date.getMonth() + "/"; // Añado el mes
    fechaFinal+=date.getFullYear() + " ";
    fechaFinal+=(date.getHours() < 10 ? '0' : '') + date.getHours() + ":";
    fechaFinal+=(date.getMinutes() < 10 ? '0' : '') + date.getMinutes();

    return fechaFinal
}

/**
 * Recojo todos los mensajes del chat ya existentes y los pinto en en pantalla
 * @param idGrupo
 * @returns {Promise<void>}
 */
async function abrirChat(idGrupo){
    localStorage.setItem("idGrupo", idGrupo); // Guardo esto para utilizarlo al mandar el mensaje
    recibirMensajes();
    document.getElementById("gruposBusqueda").style.display = "none";
    document.getElementById("divMensajesBusqueda").style.display = "block";

    // Recojo todos los mensajes del grupo
    let res = await axios.get('http://localhost:3000/mensajes/'+idGrupo);
    var data = JSON.parse(JSON.stringify(res.data)).json;

    // Recojo todos los mensajes que hay guardados en la base de datos
    if(data[0]!=undefined) {
        data[0].mensajes.forEach(function (valor, indice, array) {
            pintarMensaje(valor.mensaje, valor.id_usuario, valor.fechaYHora.substring(11, 19), valor.nombre, valor.esDestacado)
        });
    }
}

// Cuando doy a intro envía el mensaje
$("#mensaje").keyup(function(event) {
    if (event.keyCode === 13) {
        $("#enviar").click();
    }
});

/**
 * Manda el mensaje al servidor de sockets
 * @param msg
 */
function enviarMensaje(msg){
    var mensaje = {
        "idGrupo":localStorage.getItem("idGrupo"),
        "mensaje":msg,
        "idUsuario":"Administrador",
        "nombre":"Administrador"
    }
    chat.emit('mensaje', mensaje);
    document.getElementById("mensaje").value=""
}

/**
 * Escucha de mensajes en el socket
 */
function recibirMensajes(){
    console.log(localStorage.getItem("idGrupo"))
    chat.on(localStorage.getItem("idGrupo")+"msg", function(msg, idUsuario, hora, nombre, esDestacado){
        pintarMensaje(msg, idUsuario, hora, nombre, esDestacado);
    });
}

// Pinta en el html los mensajes formateados
function pintarMensaje(mensaje, idUsuario, hora, nombre, destacado){
    var li = document.createElement("li");
    li.classList.add("list-group-header"); // Le doy la clase del li

    if(destacado == true){ // Si es un mensaje destacado el estilo es azul
        li.classList.add("msgDestacado"); // Le doy la clase del li
    }else if(idUsuario=="Administrador"){ // Si el mensaje lo ha enviado el emisor el estilo del mensaje es diferente
        li.classList.add("msgEmisor"); // Le doy la clase del li
    }else{
        li.classList.add("msg"); // Le doy la clase del li
    }


    var divv = document.createElement("div");
    divv.classList.add("media-body"); // Le doy la clase del div

    var strong = document.createElement("strong");
    strong.classList.add("ms"); // Le doy la clase del img

    var sData = document.createTextNode(nombre + " ~" + hora);

    var par = document.createElement("p");
    var pData = document.createTextNode(mensaje);

    li.appendChild(divv);
    divv.appendChild(strong);
    strong.appendChild(sData);
    divv.appendChild(par);
    par.appendChild(pData);

    var element = document.getElementById("mensajesBusqueda");
    element.appendChild(li);
}

function volverGrupo(){
    document.getElementById("gruposBusqueda").style.display = "block";
    document.getElementById("divMensajesBusqueda").style.display = "none";
    localStorage.removeItem('idGrupo');
}