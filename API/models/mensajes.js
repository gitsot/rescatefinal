var mongoose = require('mongoose');
//var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var mensajesSchema = new Schema({
    mensaje: { type: [JSON], required: [true, 'El mensaje es obligatorio'] }, //aquí vienen 5 parápetros en el json, msg, fechaYHora, esDestacado, idUsuario y el nombre del usuario
    id_grupo:{ type:Schema.Types.ObjectId, ref: 'grupo' },
});

//pacienteSchema.plugin( uniqueValidator, { message: 'el {PATH} debe ser único' });
module.exports = mongoose.model('mensajes', mensajesSchema);