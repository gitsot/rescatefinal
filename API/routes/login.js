var express = require('express');
var app = express();
var loginController = require('../controllers/loginController.js')

app.post('/', loginController.logUsuario);

module.exports = app;