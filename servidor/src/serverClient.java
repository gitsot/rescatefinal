import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.io.InputStream;
import java.net.InetSocketAddress;

public class serverClient {
	
    //Arranca el proceso
    public static void main(String[] args) {
        try {
            String ip = "localhost";
            int port = 3333;

            Socket s = new Socket();
            InputStream is = null;
            BufferedReader in = null; 
            PrintWriter out = null;

            System.out.println("Mi server en:" +ip+ ":"+ port + "\n");
            InetSocketAddress server = new InetSocketAddress(ip, port);

            s.connect(server);
            System.out.println("connect en (" + new Date() + ")");

            is = s.getInputStream();
            in = new BufferedReader(new InputStreamReader(is));
            out = new PrintWriter(s.getOutputStream());

            for(int i=0; i<5; i++){
	            out.println(i);
	            out.flush(); // Env�a al servidor

	            out.println(i*10);
	            out.flush();

	            // Respuesta del servidor
	            // Importante: Si escribo en Printer/Leo Line a Line
	            String input = in.readLine();

	            System.out.println(input);
            }
            
            out.println("END");
            out.flush();

            in.close();
            out.close();
            s.close();
        } catch (IOException | NumberFormatException | ArrayIndexOutOfBoundsException e) { 
            System.out.println("error:");
            System.err.println(e.getMessage());
        } 
    }
}
