var mongoose = require('mongoose');
//var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var busquedaSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es obligatorio'] },
    descripcion: { type: String, required: [true, 'La descripción es obligatoria'] },
    nombreZona: { type: String, required: [true, 'el nombre de la zona es obligatoria'] },
    fechaCreado: { type: Date, required: [true, 'La fecha es obligatoria'] },
    coordenadas: { type: {String}, required: [true, 'Las coordenadas son obligatorias'] },
    marcadores: { type: [JSON] },
    esActiva: { type: Boolean },
});

//pacienteSchema.plugin( uniqueValidator, { message: 'el {PATH} debe ser único' });
module.exports = mongoose.model('busqueda', busquedaSchema);