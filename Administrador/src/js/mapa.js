const { remote } = require('electron');

/**
 * Abre la ventana modal del mapa
 */
function openModal(coorsCirculo, coorsPoligono, url) {
    if(coorsCirculo){ // Si estoy modificando ya existen unas coordenadas y las pinto en el mapa
        localStorage.setItem('coordenadasModificar', coorsCirculo); // Guardo en local para obtenerlo después
    }
    if(coorsPoligono){ // Si estoy modificando ya existen unas coordenadas y las pinto en el mapa
        localStorage.setItem('poligonoVisualizar', coorsPoligono); // Guardo en local para obtenerlo después
    }

    let win = new remote.BrowserWindow({
        width: 600,
        height: 446,
        frame: false,
        parent: remote.getCurrentWindow(),
        modal: true
    })

    var theUrl = `file://${__dirname}/../mapa/${url}.html`;

    win.loadURL(theUrl);

    document.getElementById("ardid").className = "btn btn-large btn-positive";
}

function guardaCoordenadas(coordenadas){
    localStorage.setItem('coordenadas', coordenadas); // Guardo en local para obtenerlo después
    window.close();
}


