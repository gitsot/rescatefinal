var Grupo = require('../models/grupo');
var Usuario = require('../models/usuario');
var comun = require('./comunController');
var usuarioController = require('./usuarioController');
var Mensajes = require('../models/mensajes');

/**
 * POST GRUPO
 * Crea una busqueda
 */

function postGrupo(req, res, next){
    var body = req.body;

    var grupo = new Grupo({
        nombre: body.nombre,
        coordenadas: [JSON.parse(body.coordenadas[0])],
        fechaCreado: new Date(),
        id_busqueda: body.id_busqueda,
        id_usuario: [],
    });

    grupo.save( ( err, grupoGuardado ) => {
        if (err){
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear el grupo',
                errors: err
            });
        }

        // Creo los mensajes vacíos de ese grupo
        var mensaje = new Mensajes({
            id_grupo: grupoGuardado._id,
        });

        mensaje.save( ( err, mensajeGuardado ) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al crear el mensaje',
                    errors: err
                });
            }
        });

        /*res.status(201).json({
            ok: true,
            grupo: grupoGuardado
        });*/
    });
}

/**
 * GET GRUPOS BUSQUEDA
 * Devuelve los grupos de una busqueda
 * @param req
 * @param res
 * @param next
 */
function getGruposBusqueda(req, res, next){
    Grupo.find({ id_busqueda : req.params.idBusqueda })
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    json
                });
            });
}

/**
 * GET GRUPOS BUSQUEDA
 * Devuelve las coordenadas de los grupos de una busqueda
 * @param req
 * @param res
 * @param next
 */
function getGruposBusquedaCoordenadas(req, res, next){
    Grupo.find({ id_busqueda : req.params.idBusqueda }, 'coordenadas')
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    json
                });
            });
}

/**
 * GET GRUPOS BUSQUEDA
 * Devuelve los grupos de un usuario
 * @param req
 * @param res
 * @param next
 */
function getGruposUsuario(req, res, next){
    Grupo.find({ id_usuario : req.params.idUsuario })
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    json
                });
            });
}

/**
 * GET GRUPO
 * Devuelve el grupo que busca
 * @param req
 * @param res
 * @param next
 */
function getGrupo(req, res, next){
    Grupo.findOne({_id : req.params.id})
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    json
                });
            });
}

function getUsuarios(req, res, next){
    Usuario.find({ id_grupo : req.params.id })
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }


                res.status(200).json({
                    ok: true,
                    json
                });
            });
}


function putGrupo(req, res, next){
    var id = req.params.id;
    var body = req.body;

    Grupo.findById(id, (err, grupo)=>{
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar el grupo',
                errors: err
            });
        }

        if (!grupo){
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Ese grupo no existe',
                    errors: err
                });
            }
        }

        grupo.nombre = body.nombre;
        grupo.coordenadas = [JSON.parse(body.coordenadas[0])];
        grupo.fechaCreado = grupo.fechaCreado // Mantengo la fecha de creado porque no se puede modificar
        grupo.id_busqueda = grupo.id_busqueda // Mantengo el id de grupo

        grupo.save( ( err, grupoGuardado ) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar el usuario',
                    errors: err
                });
            }

            /*res.status(200).json({
                ok: true,
                paciente: busquedaGuardada
            });*/
        });
    });
}

/**
 * DELETE GRUPO
 * Borra un grupo
 * @param req
 * @param res
 * @param next
 */
function deleteGrupo(req, res, next){
    var id = req.params.id;

    Grupo.findByIdAndRemove(id, (err, GrupoBorrado)=>{
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar la búsqueda',
                errors: err
            });
        }

        if (!GrupoBorrado){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe esa búsqueda con ese id',
            });
        }

        // Borro las relaciones que tienen con los usuarios asignados
        GrupoBorrado.id_usuario.forEach( function(valor, indice, array) {
            comun.borrarRelacionUsuario(valor, id)
        });

        /*res.status(200).json({
            ok: true,
            consulta: GrupoBorrado
        });*/
    });
}

function deleteRelationGrupo(req, res, next){
    // Borro la relacion en el grupo
    comun.borrarRelacionGrupo(req.params.idGrupo, req.params.idUsuario);

    // Borro la relacion en el usuario
    comun.borrarRelacionUsuario(req.params.idUsuario, req.params.idGrupo);
}

function perteneceBusqueda(req, res, next){
    Grupo.find({$and : [{ id_busqueda : req.params.idBusqueda }, {id_usuario : req.params.idUsuario}]})
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                // Si la longitud es mayor a 0 es que pertenece a esa búsqueda
                if(json.length>0){
                    res.status(200).json({
                        json: true,
                    });
                }else{
                    res.status(200).json({
                        json: false,
                    });
                }



            });
}

// Comprueba si un usuario pertenece a una búsqueda, si no pertenece lo mete en el grupo con menos personas y devuelve el grupo, si ya pertenece solo devuelve el grupo
function meteGrupo(req, res, next) {
    // Comprobar si el usuario está en un grupo en esa búsqueda
    //      Si no lo está meterlo en el grupo con menos gente
    //              Si no hay grupos mostrar un toast "no hay grupos disponibles para esta búsuqueda"
    //      Abrir la pantalla de busqueda (chat y mapa)


    Grupo.find({$and : [{ id_busqueda : req.params.idBusqueda }, {id_usuario : req.params.idUsuario}]})
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                // Si la longitud es mayor a 0 es que pertenece a esa búsqueda y la devuelvo
                if(json.length>0){
                    res.status(200).json({
                        json
                    });
                }else{ // si no, meto al usuario en una búsqueda y la devuelvo (si no hay busquedas mandar un mensaje)
                    // Obtengo el grupo con menos integrantes y meto al usuario en el y después lo devuelvo
                    Grupo.find({"id_busqueda" : req.params.idBusqueda}).sort({id_usuario: 1}).limit(1)
                        .exec((err, json) => {
                            if(json.length>0) {
                                // meto al usuario en la relacion entra grupo y usuario
                                req.params.idGrupo = json[0]._id;
                                usuarioController.putAsignaGrupoUsuario(req, res, next);

                                res.status(200).json({
                                    json
                                });
                            }else{ // Si no hay búsquedas mando el json en 'no'
                                res.status(200).json({
                                    json: "no"
                                });
                            }

                        });
                }
            });
}

module.exports = {
    postGrupo,
    getGruposBusqueda,
    getGrupo,
    putGrupo,
    deleteGrupo,
    getUsuarios,
    getGruposUsuario,
    deleteRelationGrupo,
    perteneceBusqueda,
    getGruposBusquedaCoordenadas,
    meteGrupo
};