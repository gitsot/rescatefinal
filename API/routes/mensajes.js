var express = require('express');
var app = express();
var mensajeController = require('../controllers/mensajeController.js');
//var mdAutenticacion = require('../middlewares/autenticacion'); // Al usar esta variable verifica el token


app.get('/:idGrupo', /*mdAutenticacion.verificaToken,*/ mensajeController.getMensajes); // Visualiza todos los mensajes en un grupo

module.exports = app;