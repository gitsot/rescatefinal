import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

public class server implements Runnable{ 
    private Socket connect;
    private static ArrayList<Socket> clientes = new ArrayList<Socket>();
    public server(Socket client) {
        connect = client;
    }    

    public static void main(String[] args) {
        try {
        	int port = 4335;  // Inicio en el puerto 3333
        	
            ServerSocket escucha = new ServerSocket(port);
            System.out.println("escuchando en: " + port + "\n");
            
            while (true) {
            	Socket s = escucha.accept();
                server miServer = new server(s);
                System.out.println("accept en (" + new Date() + ")");
            	clientes.add(s); // Se agregan los clientes a un arraylist

                // y se auto duplica cada vez que se acepta una conexi�n                
                new Thread(miServer).start();
            }
            
        } catch (IOException | NumberFormatException | ArrayIndexOutOfBoundsException e) { 
            System.err.println(e.getMessage());
        } 
    }

    //En el thread tratamos la conexi�n
    @Override
    public void run() {
        BufferedReader in = null; 
        String request = null;

        PrintWriter out = null;

        System.out.println("Cliente: "+connect.getRemoteSocketAddress().toString());
        try {
            String input, input2 = null; 
            in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            out = new PrintWriter(connect.getOutputStream());
                
            input = in.readLine(); 
            while(input.compareTo("END")!=0){            
                System.out.println("Recibo: "+input );

              //input2 = in.readLine();
                //System.out.println("Recibo: "+input2 );
                broadcast(connect,input);
                //out.println (input);
                //out.flush();
                
                input = in.readLine(); 
            }
           
            //in.close();
            //out.close();
            //connect.close(); 
 
        } catch (IOException ioe) {
            System.err.println(ioe);
        } 
        System.out.println("...");
    }
    
    public void broadcast(Socket socket, String mensaje) throws IOException {
    	//PrintWriter out = null;
    	//System.out.println(clientes.size());
    	for(Socket cliente : clientes) {
    		//System.out.println("culo");
    		PrintWriter out = new PrintWriter(cliente.getOutputStream());
    		out.println (mensaje);
    		out.flush();
    	}
    }
}
