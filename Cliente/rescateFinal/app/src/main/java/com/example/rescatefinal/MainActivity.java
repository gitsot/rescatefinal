package com.example.rescatefinal;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.*;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity {
    EditText etUsuario;
    EditText etPassword;
    Context mContext;
    ServerSocket server;
    String ip = new ClaseGlobal().getIP();
    private NotificationManager mNotificationManager;
    private NotificationManagerCompat notificationManager;
    public static final String CHANNEL_1_ID = "channel1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUsuario = findViewById(R.id.etUsuario);
        etPassword = findViewById(R.id.etPassword);

        Button login = findViewById(R.id.bLogin);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //notificationManager.notify(1, notification);
                RealizarPost(etUsuario.getText().toString(), etPassword.getText().toString());
            }
        });
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        etUsuario.setText(savedInstanceState.getString("user"));
        etPassword.setText(savedInstanceState.getString("pssw"));
    }

    // invoked when the activity may be temporarily destroyed, save the instance state here
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("user", etUsuario.getText().toString());
        outState.putString("pssw", etPassword.getText().toString());

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }


    public void RealizarPost(final String usuario, final String password) {
        String url = "http://"+ip+":3000/login";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) { // Si el usuario es correcto me lleva a la nueva actividad
                        Intent myIntent = new Intent(MainActivity.this, listaBusquedas.class);
                        myIntent.putExtra("usuario", response);
                        startActivity(myIntent);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Muestro mensaje de error en pantalla
                        Toast toastError = Toast.makeText(getApplicationContext(),"El usuario y contraseña no son correctos", Toast.LENGTH_SHORT);
                        toastError.setGravity(Gravity.TOP, 0, 150);
                        toastError.show();

                        //Vacío los textBox para volver a introducir
                        etUsuario.setText("");
                        etPassword.setText("");

                        // Muestro error por línea de comandos
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                // the POST parameters:
                params.put("dni", usuario);
                params.put("password", password);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(postRequest);
    }
}

