package com.example.rescatefinal;

import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.example.rescatefinal.ui.main.SectionsPagerAdapter;
import org.json.JSONException;
import org.json.JSONObject;

public class busqueda extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    public JSONObject getUsu() throws JSONException {
        String usuario = getIntent().getStringExtra("usuario");
        JSONObject jsonObjectUsu = new JSONObject(usuario);
        return (JSONObject) jsonObjectUsu.get("json");
    }

    public String getIdUsuario() throws JSONException {
        JSONObject usu = getUsu();
        return usu.get("_id").toString();
    }

    public JSONObject getGru() throws JSONException {
        String grupo = getIntent().getStringExtra("grupo");
        return new JSONObject(grupo);
    }

    public String getIdGrupo() throws JSONException {
        JSONObject gru = getGru();
        return gru.get("_id").toString();
    }

    public JSONObject getBus() throws JSONException {
        String busqueda = getIntent().getStringExtra("busqueda");
        return new JSONObject(busqueda);
    }

    public String getIdBusqueda() throws JSONException {
        JSONObject bus = getBus();
        return bus.get("_id").toString();
    }
}