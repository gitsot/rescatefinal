package com.example.rescatefinal.ui.main;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.example.rescatefinal.ClaseGlobal;
import com.example.rescatefinal.R;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {
    private Socket socket;
    private JSONObject usu;
    private JSONObject gru;
    private JSONObject bus;
    private String idUsuario;
    private String idBusqueda;
    private String idGrupo;
    String ip = new ClaseGlobal().getIP();

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2};
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        try {
            socket = IO.socket("http://"+ip+":4000");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        /// getItem is called to instantiate the fragment for the given page.
        // Return a ClusterFragment (defined as a static inner class below).

        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new MapFragment(socket);
                break;
            case 1:
                //fragment = PlaceholderFragment.newInstance(position + 1);
                fragment = new chatFragment(socket);
                break;
            default:
                break;
        }

        return fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "MAPA";
            case 1:
                return "CHAT";
        }
        return null;
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }
}