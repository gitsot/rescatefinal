function cierraModal() {
    window.close()
    localStorage.removeItem("coordenadas"); // Elimino el local storage si lo hubiera
}

function muestraPopup() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
    ocultaPopup(); // Muestro el popup de usuario creado y a los dos segunos lo oculto
}

function ocultaPopup() {
    setTimeout(function(){
        var popup = document.getElementById("myPopup");
        popup.classList.toggle("show");
    }, 2000);
}