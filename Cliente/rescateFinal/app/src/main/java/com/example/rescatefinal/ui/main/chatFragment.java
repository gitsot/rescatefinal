package com.example.rescatefinal.ui.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.rescatefinal.ClaseGlobal;
import com.example.rescatefinal.R;
import com.example.rescatefinal.busqueda;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class chatFragment extends Fragment {
    Context context;
    CardView cardview;
    LinearLayout.LayoutParams layoutparamsCard;
    LinearLayout.LayoutParams layoutparamsLinearCard1;
    LinearLayout.LayoutParams layoutparamsLinearCard2;
    LinearLayout.LayoutParams layoutparamsTextView1;
    LinearLayout.LayoutParams layoutparamsTextView2;
    LinearLayout.LayoutParams layoutparamsImage;
    TextView textviewUsu;
    TextView textviewPass;
    //LinearLayout linearLayout;
    LinearLayout linearLayoutCard1;
    LinearLayout linearLayoutCard2;
    ImageView imageView;
    //Intent myIntent;
    View rootView;
    Socket socket;
    private JSONObject usu;
    private JSONObject gru;
    private JSONObject bus;
    private String idUsuario;
    private String idBusqueda;
    private String idGrupo;
    ScrollView sv;
    String ip = new ClaseGlobal().getIP();

    public chatFragment(Socket socket){
        this.socket = socket;
        socket.connect();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            usu = ((busqueda) getActivity()).getUsu();
            idUsuario = ((busqueda) getActivity()).getIdUsuario();

            gru = ((busqueda) getActivity()).getGru();
            idGrupo = ((busqueda) getActivity()).getIdGrupo();

            bus = ((busqueda) getActivity()).getBus();
            idBusqueda = ((busqueda) getActivity()).getIdBusqueda();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    public void scroll(){
        sv.postDelayed(new Runnable() {
            @Override
            public void run() {
                sv.fullScroll(ScrollView.FOCUS_DOWN);
            }
        }, 100);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        sv = (ScrollView) rootView.findViewById(R.id.scrollViewTopicGrade);

        // Recojo los mensajes en la base de datos
        cargarMensajes();
        // Para hacer scroll necesito hacer un post al scroll
        scroll();

        recibeMensaje();

        Button b = rootView.findViewById(R.id.push_button);
        final EditText message = rootView.findViewById(R.id.etMensaje);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!message.getText().toString().equals("")) { // Evito mandar un mensaje vacío
                    // Envío el mensaje por socket en un json
                    JSONObject mensaje = new JSONObject();
                    try {
                        mensaje.put("mensaje", message.getText().toString());
                        mensaje.put("idGrupo", idGrupo);
                        mensaje.put("idUsuario", idUsuario);
                        mensaje.put("nombre", usu.get("nombre"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    socket.emit("mensaje", mensaje);
                    message.getText().clear();
                }
                scroll();
            }
        });

        return rootView;
    }

    /**
     * Pinto un mensaje en pantalla
     */
    public void getMessages(String msg, String msgIdUsu, String hora, String nombre, boolean destacado){
        LinearLayout linearLayout = rootView.findViewById(R.id.mensajes);

        // Creo el CardView
        cardview = new CardView(getActivity());
        layoutparamsCard = new LinearLayout.LayoutParams(
                900,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        layoutparamsCard.setMargins(20,0,20,20);
        cardview.setRadius(20);
        cardview.setLayoutParams(layoutparamsCard);

        if(destacado == true){
            cardview.setCardBackgroundColor(Color.parseColor("#90ffff"));
            cardview.getLayoutParams().width=ViewGroup.LayoutParams.MATCH_PARENT;
        }else if(msgIdUsu.equals(idUsuario)){ // Si el mensaje enviado es el del usuario se pone en color verde y con otro formato
            cardview.setCardBackgroundColor(Color.parseColor("#ade580"));
            layoutparamsCard.gravity = Gravity.RIGHT;
        }else{// Si no, en color blanco
            cardview.setCardBackgroundColor(Color.WHITE);
        }
        // --------------------------------------------------

        // Creo el primer LinearLayout
        linearLayoutCard1 = new LinearLayout(getActivity());
        layoutparamsLinearCard1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        linearLayoutCard1.setOrientation(LinearLayout.HORIZONTAL);
        linearLayoutCard1.setLayoutParams(layoutparamsLinearCard1);
        // --------------------------------------------------

        // Creo el segundo LinearLayout
        linearLayoutCard2 = new LinearLayout(getActivity());
        layoutparamsLinearCard2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        layoutparamsLinearCard2.setMargins(10,10,10,10);
        linearLayoutCard2.setOrientation(LinearLayout.VERTICAL);
        linearLayoutCard2.setLayoutParams(layoutparamsLinearCard2);
        // --------------------------------------------------

        // Creo el primer textView el del nombre
        layoutparamsTextView1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        textviewUsu = new TextView(getActivity());
        textviewUsu.setTypeface(null, Typeface.BOLD);
        textviewUsu.setText(nombre + " ~" + hora);
        textviewUsu.setTextSize(11);

        if(destacado == true){
            textviewUsu.setTextColor(Color.parseColor("#35682d"));
        }else if(msgIdUsu.equals(idUsuario)){ // Si lo manda el propio usuario el texto va a ser de otro color
            textviewUsu.setTextColor(Color.parseColor("#5F36C6"));
        }else{
            textviewUsu.setTextColor(Color.parseColor("#278dc4"));
        }
        layoutparamsTextView1.setMargins(0,10,0,0);
        textviewUsu.setLayoutParams(layoutparamsTextView1);
        // --------------------------------------------------

        // Creo el segundo textView, el de descripción
        layoutparamsTextView2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        textviewPass = new TextView(getActivity());
        textviewPass.setText(msg);
        textviewPass.setTextSize(18);
        textviewPass.setTextColor(Color.BLACK);
        layoutparamsTextView2.setMargins(10,0,10,15);
        textviewPass.setLayoutParams(layoutparamsTextView2);
        // --------------------------------------------------

        linearLayoutCard2.addView(textviewUsu);

        linearLayoutCard2.addView(textviewPass);

        linearLayoutCard1.addView(linearLayoutCard2);

        cardview.addView(linearLayoutCard1);

        linearLayout.addView(cardview);

    }

    /**
     * Hilo de escucha para recibir los mensajes de las demás personas
     */
    public void recibeMensaje(){
        //final Marker[] spy = {null};
        socket.on(idGrupo.concat("msg"), new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getMessages(args[0].toString(), args[1].toString(), args[2].toString(), args[3].toString(), (boolean) args[4]); // Pinto el mensaje en el chat
                        scroll();
                    }
                });
            }
        });
    }

    /**
     * Descarga todos los mensajes de ese grupo y los pinta en pantalla al iniciar sesión
     */
    public void cargarMensajes(){
        String url = "http://"+ip+":3000/mensajes/"+idGrupo;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray json = jsonObject.getJSONArray("json");
                            JSONObject json0 = (JSONObject) json.get(0);
                            JSONArray mensajes = json0.getJSONArray("mensajes");

                            // Pinto los mensajes uno a uno
                            for (int x = 0; x < mensajes.length(); x++) {
                                JSONObject mensaje = (JSONObject) mensajes.get(x);
                                String hora = mensaje.get("fechaYHora").toString().substring(11, 19);

                                getMessages(mensaje.get("mensaje").toString(), mensaje.get("id_usuario").toString(), hora, mensaje.get("nombre").toString(), (Boolean) mensaje.get("esDestacado"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        Volley.newRequestQueue( getActivity()).add(postRequest);

    }
}
