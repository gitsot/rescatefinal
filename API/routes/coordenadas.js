var express = require('express');
var app = express();
var coordenadaController = require('../controllers/coordenadaController.js');
//var mdAutenticacion = require('../middlewares/autenticacion'); // Al usar esta variable verifica el token


app.get('/:idGrupo', /*mdAutenticacion.verificaToken,*/ coordenadaController.getTracks); // Visualiza las coordenadas de un grupo
app.get('/:idGrupo/:idUsuario', /*mdAutenticacion.verificaToken,*/ coordenadaController.getTracksUsuarioGrupo); // Visualiza todas las coordenadas de un usuario en un grupo en concreto

module.exports = app;