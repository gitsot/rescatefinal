const axios = require('axios');

/**
 * Crea cualquier desplegable recibiendo usuarios, grupos o búsquedas
 * @param url
 * @param tipo
 * @returns {Promise<void>}
 */
async function crearDesplegable(url, tipo) {
    // Recojo de la API los nombres y los ids mandandole el parametro de busqueda por url
    let res = await axios.get(url);
    var data = JSON.parse(JSON.stringify(res.data));

    // Recorro el array para añadir en el desplegable las busquedas activas
    var busActiv = document.getElementById(tipo);
    data.json.forEach( function(valor, indice, array) {
        var option = document.createElement("option");
        option.text = valor.nombre;
        option.value = valor._id;
        //option.selected = "selected";
        busActiv.add(option);
    });
}
