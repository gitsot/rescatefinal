var Coordenadas = require('../models/coordenadas');
var Mensajes = require('../models/mensajes');
var Busqueda = require('../models/busqueda');
var Grupo = require('../models/grupo');

function localizacion(coors){
    // Necesito buscar las coordenadas con ESE usuario y modificarlas
    Coordenadas.findOne({$and: [{"id_Usuario" : coors.idUsuario}, {"id_Grupo" : coors.idGrupo}] })
        .exec((err, json) => {
            if (err){
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error de base de datos',
                    errors: err
                });
            }

            json.coordenadas.push(coors.coors);

            json.save( ( err, coordenadasAnadidias ) => {
                if (err){
                    return res.status(400).json({
                        ok: false,
                        mensaje: 'Error al actualizar las coordenadas',
                        errors: err
                    });
                }
            });
        });
}

var userCoor = [];
function trackBroadcast(coors){
    if(Object.keys(userCoor).indexOf(coors.idUsuario)===-1){ // Si no tengo guardadas las coordenadas previas las guardo
        userCoor[coors.idUsuario] = coors.coors;
    }

    obj = {
        x1: coors.coors.x, // Coordenada x nueva
        y1: coors.coors.y, // Coordenada y nueva
        x2: userCoor[coors.idUsuario].x,  // Coordenada x anterior
        y2: userCoor[coors.idUsuario].y,   // Coordenada y anterior
        idUsu: coors.idUsuario, // Mando también al usuario
        nombre: coors.nombre, // Cojo el nombre
        appParada: coors.appParada // Cojo si la App se ha parado
    };
    userCoor[coors.idUsuario] = coors.coors;

    if(coors.appParada){ // Si se ha parado la aplicación elimino la coordenada anterior del array
        delete userCoor[coors.idUsuario]
    }
    return obj;
}

function chat(msg){
    // Necesito buscar las coordenadas con ESE usuario y modificarlas
    Mensajes.findOne({"id_grupo" : msg.idGrupo})
        .exec((err, json) => {
            if (err){
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error de base de datos',
                    errors: err
                });
            }

            if(msg.esDestacado==undefined){
                msg.esDestacado=false;
            }
            var mensaje ={
                "mensaje":msg.mensaje,
                "fechaYHora": new Date(),
                "esDestacado": msg.esDestacado,
                "id_usuario":  msg.idUsuario,
                "nombre":  msg.nombre
            };

            json.mensaje.push(mensaje);

            json.save( ( err, mensajeAnadido ) => {
                if (err){
                    return res.status(400).json({
                        ok: false,
                        mensaje: 'Error al actualizar los mensajes',
                        errors: err
                    });
                }
            });
        });
}

function marcador(marc, idBusqueda){
// Necesito buscar la búsqueda
    Busqueda.findOne({"_id" : idBusqueda})
        .exec((err, json) => {
            if (err){
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error de base de datos',
                    errors: err
                });
            }

            var marcador = {
                "idUsuario": marc.idUsuario,
                "descripcion":marc.descripcion,
                "coordenadas": marc.coordenadas
            };

            json.marcadores.push(marcador);

            json.save( ( err, marcadorAnadido ) => {
                if (err){
                    return res.status(400).json({
                        ok: false,
                        mensaje: 'Error al actualizar los mensajes',
                        errors: err
                    });
                }
            });
        });
}

function EnviarDestacadoABusqueda(idBusqueda, msg, socket, hora){
    Grupo.find({ id_busqueda : idBusqueda }, "_id")
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                json.forEach( function(valor, indice, array) {
                    msg.idGrupo=valor._id;
                    msg.esDestacado=true;
                    chat(msg); // Guardo el mensaje en la base de datos
                    socket.broadcast.emit(valor._id+"msg", msg.mensaje, msg.idUsuario, hora, msg.nombre, true); // Con esto estoy mandando los mensajes a TODOS los del grupo
                    socket.emit(valor._id+"msg", msg.mensaje, msg.idUsuario, hora, msg.nombre, true); // y me lo mando a mi mismo
                });
            });
}

module.exports = {
    localizacion,
    trackBroadcast,
    chat,
    marcador,
    EnviarDestacadoABusqueda
};