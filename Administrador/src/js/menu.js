const { BrowserWindow } = require("electron");

/**
 *
 * browserWindow, la panatalla padre
 * width, anchura pantalla modal
 * height altura pantalla modal
 * url, url del código a cargar en el proceso renderer
 */
function createFormModal(browserWindow, width, height, url) {
    let winForm = new BrowserWindow({
        width: width,
        height: height,
        frame: false,
        parent: browserWindow,
        modal: true
    })
    winForm.loadURL(url)
}

// browserWindow es la pantalla sobre la que se quiere crear el menú
let mainMenu = browserWindow => {
    let templateMenu = [
        {
            label: "Usuarios",
            submenu: [
                {
                    label: "Crear usuario",
                    accelerator: "CommandOrControl+C",
                    click() {
                        let url = `file://${__dirname}/../html/usuario/crearUsuario.html`;
                        createFormModal(browserWindow, 410, 430, url);
                    }
                },
                {
                    label: "Modificar usuario",
                    accelerator: "CommandOrControl+U",
                    click() {
                        let url = `file://${__dirname}/../html/usuario/modificarUsuario.html`;
                        createFormModal(browserWindow, 420, 500, url);
                    }
                },
                {
                    label: "Ver usuario",
                    accelerator: "CommandOrControl+Z",
                    click() {
                        let url = `file://${__dirname}/../html/usuario/verUsuario.html`;
                        createFormModal(browserWindow, 420, 490, url);
                    }
                },
                {
                    label: "Borrar usuario",
                    accelerator: "CommandOrControl+T",
                    click() {
                        let url = `file://${__dirname}/../html/usuario/borrarUsuario.html`;
                        createFormModal(browserWindow, 400, 470, url);
                    }
                },
                {
                    label: "Asignar usuario a grupo",
                    accelerator: "CommandOrControl+Y",
                    click() {
                        let url = `file://${__dirname}/../html/usuario/asignarGrupo.html`;
                        createFormModal(browserWindow, 400, 280, url);
                    }
                },
                {
                    label: "Borrar usuario de grupo",
                    accelerator: "CommandOrControl+P",
                    click() {
                        let url = `file://${__dirname}/../html/usuario/borrarUsuGrupo.html`;
                        createFormModal(browserWindow, 400, 220, url);
                    }
                },
                /*{
                    label: "salir",
                    role: "quit"
                }*/
            ]
        },
        {
            label: "Búsquedas",
            submenu: [
                {
                    label: "Crear búsqueda",
                    accelerator: "CommandOrControl+B",
                    click() {
                        let url = `file://${__dirname}/../html/busqueda/crearBusqueda.html`;
                        createFormModal(browserWindow, 420, 355, url);
                    }
                },
                {
                    label: "Modificar búsqueda",
                    accelerator: "CommandOrControl+G",
                    click() {
                        let url = `file://${__dirname}/../html/busqueda/modificarBusqueda.html`;
                        createFormModal(browserWindow, 420, 465, url);
                    }
                },
                {
                    label: "Borrar búsqueda",
                    accelerator: "CommandOrControl+M",
                    click() {
                        let url = `file://${__dirname}/../html/busqueda/borrarBusqueda.html`;
                        createFormModal(browserWindow, 420, 510, url);
                    }
                }
            ]
        },
        {
            label: "Grupos",
            submenu: [
                {
                    label: "Crear grupo",
                    accelerator: "CommandOrControl+P",
                    click() {
                        let url = `file://${__dirname}/../html/grupo/crearGrupo.html`;
                        createFormModal(browserWindow, 420, 270, url);
                    }
                },
                {
                    label: "Modificar grupo",
                    accelerator: "CommandOrControl+O",
                    click() {
                        let url = `file://${__dirname}/../html/grupo/modificarGrupo.html`;
                        createFormModal(browserWindow, 420, 330, url);
                    }
                },
                {
                    label: "Borrar grupo",
                    accelerator: "CommandOrControl+0",
                    click() {
                        let url = `file://${__dirname}/../html/grupo/borrarGrupo.html`;
                        createFormModal(browserWindow, 420, 490, url);
                    }
                },
                {
                    label: "Ver usuarios de grupo",
                    accelerator: "CommandOrControl+J",
                    click() {
                        let url = `file://${__dirname}/../html/grupo/verUsuarios.html`;
                        createFormModal(browserWindow, 400, 550, url);
                    }
                },
            ]
        },
        {
            label: "View",
            submenu: [
                { role: "reload" },
                { role: "forcereload" },
                { role: "toggledevtools" },
                { type: "separator" },
                { role: "resetzoom" },
                { role: "zoomin" },
                { role: "zoomout" },
                { type: "separator" },
                { role: "togglefullscreen" }
            ]
        }
    ];

    return templateMenu;
};

module.exports.mainMenu = mainMenu;