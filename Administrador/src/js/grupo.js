/**
 * Carga la búsqueda elegida y la ruta para modificar esa búsqueda
 * @param selectObject
 * @returns {Promise<void>}
 */
async function cargarGrupoModificar(selectObject) {
    // pongo el botón en verde para indicar que hay una coordenada guardada
    document.getElementById("ardid").className = "btn btn-large btn-positive";

    // Recojo de la API los datos del grupo y la pinto en los texbox
    let res = await axios.get('http://localhost:3000/grupo/'+selectObject.value);
    var data = JSON.parse(JSON.stringify(res.data)).json;

    document.getElementById("nombre").value = data.nombre;

    // Recojo de la API los datos del grupo y la pinto en los texbox
    let res2 = await axios.get('http://localhost:3000/grupo/listaBusquedaCoordenadas/'+data.id_busqueda);
    var data2 = JSON.parse(JSON.stringify(res2.data)).json;
    var p=[];
    data2.forEach( function(valor, indice, array) {
        p.push(JSON.stringify(valor.coordenadas[0]))
    });

    document.getElementById("frmCoordenadasPoligono").value = JSON.stringify(p);

    // Obtengo los datos de la busqueda para mostrar la información
    let resBus = await axios.get('http://localhost:3000/busqueda/'+data.id_busqueda);
    var dataBus = JSON.parse(JSON.stringify(resBus.data)).json;

    document.getElementById("frmCoordenadas").value = JSON.stringify(dataBus.coordenadas);

    // Modifico la ruta para elegir el id correcto
    document.getElementById('form').action = "http://localhost:3000/grupo/"+data._id+"?_method=PUT";
}

/**
 * Carga el grupo elegido y la ruta para borrar ese grupo
 * @param selectObject
 * @returns {Promise<void>}
 */
async function cargarGrupoBorrar(selectObject) {
    localStorage.setItem("idGrupoVisualizar", selectObject.value);
    // pongo el botón en verde para indicar que hay una coordenada guardada
    document.getElementById("ardid").className = "btn btn-large btn-positive";

    // Recojo de la API los datos del grupo y la pinto en los texbox
    let res = await axios.get('http://localhost:3000/grupo/'+selectObject.value);
    var data = JSON.parse(JSON.stringify(res.data)).json;

    // Obtengo los datos de la busqueda para mostrar la información
    let resBus = await axios.get('http://localhost:3000/busqueda/'+data.id_busqueda);
    var dataBus = JSON.parse(JSON.stringify(resBus.data)).json;


    document.getElementById("nombre").innerHTML = data.nombre;
    document.getElementById("fechaCreado").innerHTML = data.fechaCreado;
    document.getElementById("nombreZona").innerHTML = dataBus.nombreZona;

    // Recojo de la API los datos del grupo y la pinto en los texbox
    let res2 = await axios.get('http://localhost:3000/grupo/listaBusquedaCoordenadas/'+dataBus._id);
    var data2 = JSON.parse(JSON.stringify(res2.data)).json;
    var p=[];
    data2.forEach( function(valor, indice, array) {
        p.push(JSON.stringify(valor.coordenadas[0]))
    });

    document.getElementById("frmCoordenadasPoligono").value = JSON.stringify(p);
    document.getElementById("frmCoordenadas").value = JSON.stringify(dataBus.coordenadas);

    // Modifico la ruta para elegir el id correcto
    document.getElementById('form').action = "http://localhost:3000/grupo/borrar/"+data._id;
}

/**
 * Vacía el desplegable de los grupos al hacer submit
 */
function setOptionGrupo(){
    var x = document.getElementById("grup");
    for (var i = 0; i < x.length+1; i++) {
        x.remove(1);
    }

    // pongo el botón en rojo para indicar que no hay coordenadas guardadas
    document.getElementById("ardid").className = "btn btn-large btn-negative";
}

/**
 * Pone en blanco las etiquetas al borrar el grupo
 */
function labelBlankGrupo(){
    // Elimino del desplegable la opción que acabo de eliminar
    var x = document.getElementById("grup"); // ESTO NO TIENE QUE SER NECESARIO, DEBERÍA QUEDARSE EN BLANCO Y VOLVER A CARGAR DESPUÉS
    x.remove(x.selectedIndex);

    // Vuelvo a poner en blando todos los valores
    document.getElementById("nombreZona").innerHTML = "";
    document.getElementById("nombre").innerHTML = "";
    document.getElementById("zona").innerHTML = "";
    document.getElementById("fechaCreado").innerHTML = "";
}

/**
 * Muestra en las etiquetas la información del grupo y de la zona de búsqueda
 * @param selectObject
 * @returns {Promise<void>}
 */
async function cargarUsuariosGrupo(selectObject) {
    localStorage.setItem("idGrupoVisualizar", selectObject.value);
    // pongo el botón en verde para indicar que hay una coordenada guardada
    document.getElementById("ardid").className = "btn btn-large btn-positive";

    // Recojo de la API los datos del grupo y la pinto en los texbox
    let res = await axios.get('http://localhost:3000/grupo/'+selectObject.value);
    var data = JSON.parse(JSON.stringify(res.data)).json;

    // Obtengo los datos de la busqueda para mostrar la información
    let resBus = await axios.get('http://localhost:3000/busqueda/'+data.id_busqueda);
    var dataBus = JSON.parse(JSON.stringify(resBus.data)).json;

    // Obtengo los usuarios de ese grupo
    let resUsu = await axios.get('http://localhost:3000/grupo/usuarios/'+data._id);
    var dataUsu = JSON.parse(JSON.stringify(resUsu.data)).json;

    document.getElementById("nombre").innerHTML = data.nombre;
    document.getElementById("fechaCreado").innerHTML = data.fechaCreado;
    document.getElementById("nombreZona").innerHTML = dataBus.nombreZona;

    dataUsu.forEach( function(valor, indice, array) {
        var node = document.createElement("P");
        var textnode = document.createTextNode(valor.nombre + " " + valor.apellido);
        node.appendChild(textnode);
        document.getElementById("listadoUsuarios").appendChild(node);
    });

    // Recojo de la API los datos del grupo y la pinto en los texbox
    let res2 = await axios.get('http://localhost:3000/grupo/listaBusquedaCoordenadas/'+dataBus._id);
    var data2 = JSON.parse(JSON.stringify(res2.data)).json;
    var p=[];
    data2.forEach( function(valor, indice, array) {
        p.push(JSON.stringify(valor.coordenadas[0]))
    });

    document.getElementById("frmCoordenadasPoligono").value = JSON.stringify(p);
    document.getElementById("frmCoordenadas").value = JSON.stringify(dataBus.coordenadas);
}

/**
 * Carga las coordenadas de la busqueda para pintarlas en el mapa
 * @param selectObject
 * @returns {Promise<void>}
 */
async function cargarGrupoCoors(selectObject) {
    // Recojo de la API los datos de la búsqueda y la pinto en los texbox
    let res = await axios.get('http://localhost:3000/busqueda/'+selectObject.value);
    var data = JSON.parse(JSON.stringify(res.data)).json;

    // Recojo de la API los datos del grupo y la pinto en los texbox
    let res2 = await axios.get('http://localhost:3000/grupo/listaBusquedaCoordenadas/'+data._id);
    var data2 = JSON.parse(JSON.stringify(res2.data)).json;

    var p=[];
    data2.forEach( function(valor, indice, array) {
        p.push(JSON.stringify(valor.coordenadas[0]))
    });
    document.getElementById("frmCoordenadasPoligono").value = JSON.stringify(p);
    document.getElementById("frmCoordenadas").value = JSON.stringify(data.coordenadas);

    // pongo el botón en verde para indicar que hay una coordenada guardada
    document.getElementById("ardid").className = "btn btn-large btn-positive";
}