var express = require('express');
var app = express();
var busquedaController = require('../controllers/busquedaController.js');
//var mdAutenticacion = require('../middlewares/autenticacion'); // Al usar esta variable verifica el token

app.get('/nParticipantes/:idBusqueda', /*mdAutenticacion.verificaToken,*/ busquedaController.getnParticipantesDeGrupo); // Visualiza los datos de una búsqueda
app.get('/:id', /*mdAutenticacion.verificaToken,*/ busquedaController.getBusqueda); // Visualiza los datos de una búsqueda
app.get('/horasActivo/:idBusqueda', /*mdAutenticacion.verificaToken,*/ busquedaController.getHorasBusqueda); // Visualiza en una busqueda las horas que lleva activo
app.get('/', /*mdAutenticacion.verificaToken,*/ busquedaController.getBusquedas); // Visualiza TODAS las búsquedas, posibilidad de pasar parametro ?activas=true, o false para mostrar las activas o las inactivas
app.post('/', /*mdAutenticacion.verificaToken,*/ busquedaController.postBusqueda); // Crea una busqueda
app.post('/:id', /*mdAutenticacion.verificaToken,*/ busquedaController.putBusqueda); // Modifica una búsqueda
app.post('/borrar/:id', /*mdAutenticacion.verificaToken,*/ busquedaController.deleteBusqueda); // Borra una búsqueda

module.exports = app;