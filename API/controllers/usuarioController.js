var Usuario = require('../models/usuario');
var Grupo = require('../models/grupo');
var bcrypt = require('bcryptjs');
var comun = require('./comunController');
var Coordenadas = require('../models/coordenadas');
var Mensajes = require('../models/mensajes');


/**
 * GET USUARIO
 * Muestra un usuario
 */
function getUsuario(req, res, next) {
    Usuario.findOne({ _id : req.params.id })
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    json
                });
            });
}

/**
 * GET USUARIOS
 * Muestra todos los usuarios
 */
function getUsuarios(req, res, next) {
    Usuario.find({})
        .exec((err, json) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }

        res.status(200).json({
            ok: true,
            json
        });
    });
}

/**
 * POST USUARIO
 * Crea un usuario
 */
function postUsuario(req, res, next){
    var body = req.body;

    // Guarda en body.esAdmin true o false porque al enviar el formulario un checkbox envía 'on' si se ha checkeado
    body.esAdmin = body.esAdmin !== undefined;

    var usuario = new Usuario({
        nombre: body.nombre,
        apellido: body.apellido,
        password: bcrypt.hashSync(body.password, 10),
        dni: body.dni,
        id_grupo:[],
        esAdmin: body.esAdmin,
    });

    usuario.save( ( err, usuarioGuardado ) => {
        if (err){
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear el usuario',
                errors: err
            });
        }
        /*res.status(201).json({
            ok: true,
            usuario: usuarioGuardado
        });*/
    });
}

function putAsignaGrupoUsuario(req, res, next){
    var idUsu = req.params.idUsuario;
    var idGru = req.params.idGrupo;
    var body = req.body;

    Usuario.findById(idUsu, (err, usuario)=>{
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar el usuario',
                errors: err
            });
        }

        if (!usuario){
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Ese usuario no existe',
                    errors: err
                });
            }
        }

        if(!usuario.id_grupo.includes(idGru)){ // Si ese usuario ya pertenece a ese grupo no se asigna nuevamente
            usuario.id_grupo.push(idGru);
        }

        usuario.save( ( err, usuarioGuardado ) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar el usuario',
                    errors: err
                });
            }

            /*res.status(200).json({
                ok: true,
                paciente: pacienteGuardado
            });*/
        });

        // Añadiendo al grupo...
        Grupo.findById(idGru, (err, grupo)=> {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error al buscar el grupo',
                    errors: err
                });
            }

            if (!grupo){
                if (err){
                    return res.status(400).json({
                        ok: false,
                        mensaje: 'Ese grupo no existe',
                        errors: err
                    });
                }
            }

            if(!grupo.id_usuario.includes(idUsu)){ // Si ese usuario ya pertenece a ese grupo no se asigna nuevamente
                grupo.id_usuario.push(idUsu);
            }

            grupo.save( ( err, grupoGuardado ) => {
                if (err){
                    return res.status(400).json({
                        ok: false,
                        mensaje: 'Error al actualizar el grupo',
                        errors: err
                    });
                }

                /*res.status(200).json({
                    ok: true,
                    paciente: grupoGuardado
                });*/
            });
        });

        // Creo las coordenadas vacías de ese usuario
        var coordenada = new Coordenadas({
            id_Usuario: idUsu,
            id_Grupo: idGru,
        });

        coordenada.save( ( err, coordenadaGuardada ) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al crear la coordenada',
                    errors: err
                });
            }
        });
/*
        // Creo los mensajes vacíos de ese usuario
        var mensaje = new Mensajes({
            id_usuario: idUsu,
            id_grupo: idGru,
        });

        mensaje.save( ( err, mensajeGuardado ) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al crear el mensaje',
                    errors: err
                });
            }
        });*/
    });
}

/**
 * PUT USUARIO
 * Modifica un usuario
 */
function putUsuario(req, res, next){
    var id = req.params.idUsuario;
    var body = req.body;

    Usuario.findById(id, (err, usuario)=>{
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar el usuario',
                errors: err
            });
        }

        if (!usuario){
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Ese usuario no existe',
                    errors: err
                });
            }
        }

        if(body.password==""){ // Si se envía vacía significa que no se quiere cambiar la contraseña, y si no se cifra
            var pass = usuario.password;
        }else{
            var pass = bcrypt.hashSync(body.password, 10)
        }

        usuario.nombre = body.nombre;
        usuario.apellido = body.apellido;
        usuario.password = pass;
        usuario.dni = body.dni;

        if(body.esAdmin == 'on'){ // Si el estado de admin no viene desde el formulario quiere decir que el checkbox no se ha seleccionado, así que será false (no activa)
            usuario.esAdmin = true
        }else{
            usuario.esAdmin = false
        }


        usuario.save( ( err, usuarioGuardado ) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar el usuario',
                    errors: err
                });
            }

            usuarioGuardado.password = 'Me gusta la pizza con piña'; // Esto oculta la contraseña al reenviar los datos en la respuesta

            /*res.status(200).json({
                ok: true,
                paciente: pacienteGuardado
            });*/
        });
    });
}

/**
 * DELETE USUARIO
 * Borra un usuario
 */
function deleteUsuario(req, res, next){
    var id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, UsuarioBorrado)=>{
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar el usuario',
                errors: err
            });
        }

        if (!UsuarioBorrado){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe esa búsqueda con ese id',
            });
        }

        // Borro las relaciones que tienen con los grupos asignados, los mensajes y las coordenadas
        UsuarioBorrado.id_grupo.forEach( function(valor, indice, array) {
            comun.borrarRelacionGrupo(valor, id)
            comun.borrarRelacionMensajes(valor, id)
        });
        comun.borrarRelacionCoordenadas(id)

        /*res.status(200).json({
            ok: true,
            consulta: GrupoBorrado
        });*/
    });
}

module.exports = {
    getUsuario,
    getUsuarios,
    postUsuario,
    putUsuario,
    deleteUsuario,
    putAsignaGrupoUsuario
};