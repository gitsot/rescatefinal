var Mensaje = require('../models/mensajes');
var mongoose = require('mongoose');
function getMensajes(req, res, next){

    Mensaje.aggregate([
        {
            $match: { "id_grupo" : new mongoose.Types.ObjectId(req.params.idGrupo)}

        },
        {
            $unwind: '$mensaje'
        },
        {
            $sort: {
                'mensaje.fechaYHora': 1
            }
        },
        { "$group": {"_id": "$_id", "mensajes": {$push: "$mensaje"}}}

    ]).exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }


                res.status(200).json({
                    ok: true,
                    json
                });
            });
}

/**
 * DELETE GRUPO
 * Borra un grupo
 * @param req
 * @param res
 * @param next
 */
function deleteMensaje(req, res, next){
    var id = req.params.id;

    Mensaje.findByIdAndRemove(id, (err, MensajeBorrado)=>{
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar los mensajes',
                errors: err
            });
        }

        if (!MensajeBorrado){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe esos mensajes con ese id',
            });
        }

        /*res.status(200).json({
            ok: true,
            consulta: GrupoBorrado
        });*/
    });
}

module.exports = {
    getMensajes,
    deleteMensaje
};