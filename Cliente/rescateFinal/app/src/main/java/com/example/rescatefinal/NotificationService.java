// Código encontrado en: https://www.tutorialspoint.com/send-a-notification-when-the-android-app-is-closed
package com.example.rescatefinal;

import android.app.Notification;
import android.app.Service ;
import android.content.Intent ;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.IBinder ;
import android.util.Log ;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;


import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

public class NotificationService extends Service {
    private NotificationManagerCompat notificationManager;
    public static final String CHANNEL_1_ID = "channel1";
    Socket socket;
    String TAG = "Notificacion" ;
    String ip = new ClaseGlobal().getIP();

    @Override
    public IBinder onBind (Intent arg0) {
        return null;
    }
    @Override
    public int onStartCommand (Intent intent , int flags , int startId) {
        Log. e ( TAG , "onStartCommand" ) ;
        super .onStartCommand(intent , flags , startId);
        return START_STICKY ;
    }
    @Override
    public void onCreate () {
        super.onCreate();
        try {
            socket = IO.socket("http://"+ip+":4000");
        } catch (URISyntaxException e) {
            createNotification("error") ;
            e.printStackTrace();
        }



        socket.on("notificacion", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                createNotification("SII");
            }
        });
        socket.connect();
    }

    @Override
    public void onDestroy () {
        Log. e ( TAG , "onDestroy" ) ;
        super .onDestroy() ;
    }

    private void createNotification (String titl) {
        notificationManager = NotificationManagerCompat.from(this);
        final Notification notification = new NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.locard)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                        R.drawable.locard))
                .setContentTitle(titl)
                .setContentText("Ha iniciado una nueva búsqueda, necesitamos tu colaboración.")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build();
        notificationManager.notify(1, notification);
    }
}