package com.example.rescatefinal;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.media.Image;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout.LayoutParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class listaBusquedas extends AppCompatActivity {
    Context context;
    CardView cardview;
    LayoutParams layoutparamsCard;
    LayoutParams layoutparamsLinearCard1;
    LayoutParams layoutparamsLinearCard2;
    LayoutParams layoutparamsTextView1;
    LayoutParams layoutparamsTextView2;
    LayoutParams layoutparamsImage;
    TextView textviewUsu;
    TextView textviewPass;
    LinearLayout linearLayout;
    LinearLayout linearLayoutCard1;
    LinearLayout linearLayoutCard2;
    ImageView imageView;
    Intent myIntent;
    String ip = new ClaseGlobal().getIP();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_busquedas);

        myIntent = new Intent(listaBusquedas.this, busqueda.class);
        getBusquedasActivas();
    }

    public void getBusquedasActivas() {
        String url = "http://"+ip+":3000/busqueda?activas=true";
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray busquedas = jsonObject.getJSONArray("json");
                            for (int i = 0; i < busquedas.length(); i++) {
                                JSONObject object = busquedas.getJSONObject(i);

                                // Recoge las búsquedas activas y las muestra en tarjetas
                                creaTarjetaBusqueda(object);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        Volley.newRequestQueue(this).add(postRequest);
    }

    public void inscribirBusqueda(String idBusqueda, String idUsuario) {
        String url = "http://"+ip+":3000/grupo/meteGrupo/"+idBusqueda+"/"+idUsuario;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray grupo = jsonObject.getJSONArray("json");

                            myIntent.putExtra("grupo", grupo.get(0).toString()); // Paso el grupo a la búsqueda

                            if(localizacionActivada()) { // Compruebo la localización antes de abrir el mapa
                                startActivity(myIntent); // Una vez tengo todos los datos de usuario, búsquda y del grupo abro la ventana del mapa
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        Volley.newRequestQueue(this).add(postRequest);
    }

    /**
     * Con esta función meto al usuario en un grupo de búsqueda y abro la pantalla de busqueda (chat y mapa)
     * @param busqueda
     */
    public void abrirBusqueda(JSONObject busqueda){
        String usuario = getIntent().getStringExtra("usuario");
        myIntent.putExtra("usuario", usuario); // Paso el usuario a la búsqueda
        myIntent.putExtra("busqueda", busqueda.toString()); // Paso la búsqueda a la búsqueda

        try {
            JSONObject jsonObject = new JSONObject(usuario);
            JSONObject usu = (JSONObject) jsonObject.get("json");

            // Añado la búsqueda a el siguiente activity
            inscribirBusqueda(busqueda.getString("_id"), usu.get("_id").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void creaTarjetaBusqueda(final JSONObject busqueda) throws JSONException {
        context = getApplicationContext();
        linearLayout = findViewById(R.id.linear);

        // Creo el CardView
        cardview = new CardView(context);
        cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirBusqueda(busqueda);
            }
        });
        layoutparamsCard = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT
        );
        layoutparamsCard.setMargins(20,20,20,20);

        cardview.setLayoutParams(layoutparamsCard);
        cardview.setCardBackgroundColor(Color.WHITE);
        // --------------------------------------------------

        // Creo el primer LinearLayout
        linearLayoutCard1 = new LinearLayout(context);
        layoutparamsLinearCard1 = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT
        );
        linearLayoutCard1.setOrientation(LinearLayout.HORIZONTAL);
        linearLayoutCard1.setLayoutParams(layoutparamsLinearCard1);
        // --------------------------------------------------

        // Creo el imageView
        imageView = new ImageView(context);
        imageView.setBackgroundResource(R.drawable.locard);
        layoutparamsImage = new LayoutParams(
               200, 200
        );
        layoutparamsImage.setMargins(5,5,5,5);
        imageView.setLayoutParams(layoutparamsImage);
        // --------------------------------------------------

        // Creo el segundo LinearLayout
        linearLayoutCard2 = new LinearLayout(context);
        layoutparamsLinearCard2 = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT
        );
        layoutparamsLinearCard2.setMargins(10,10,10,10);
        linearLayoutCard2.setOrientation(LinearLayout.VERTICAL);
        linearLayoutCard2.setLayoutParams(layoutparamsLinearCard2);
        // --------------------------------------------------

        // Creo el primer textView el del nombre
        layoutparamsTextView1 = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
        );

        textviewUsu = new TextView(context);
        textviewUsu.setTypeface(null, Typeface.BOLD);
        textviewUsu.setText(busqueda.getString("nombre"));
        textviewUsu.setTextSize(23);
        textviewUsu.setTextColor(Color.BLACK);
        layoutparamsTextView1.setMargins(0,10,0,0);
        textviewUsu.setLayoutParams(layoutparamsTextView1);
        // --------------------------------------------------

        // Creo el segundo textView, el de descripción
        layoutparamsTextView2 = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
        );

        textviewPass = new TextView(context);
        textviewPass.setText(busqueda.getString("descripcion"));
        textviewPass.setTextSize(13);
        textviewPass.setTextColor(Color.BLACK);
        layoutparamsTextView2.setMargins(0,0,10,15);
        textviewPass.setLayoutParams(layoutparamsTextView2);
        // --------------------------------------------------

        linearLayoutCard2.addView(textviewUsu);
        linearLayoutCard2.addView(textviewPass);

        linearLayoutCard1.addView(imageView);
        linearLayoutCard1.addView(linearLayoutCard2);

        cardview.addView(linearLayoutCard1);

        linearLayout.addView(cardview);
    }

    /**
     * Compruebo si la localización está activada antes de abrir el mapa
     */
    private boolean localizacionActivada () {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE) ;
        boolean gps_enabled = false, network_enabled = false, permissions_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER) ;
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ;
            permissions_enabled = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        } catch (Exception e) {
            e.printStackTrace() ;
        }

        if (gps_enabled && network_enabled && permissions_enabled) {
           return true;
        }else{
            Toast.makeText(getApplicationContext(),"El gps está desactivado o necesitas aceptar los permisos", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
