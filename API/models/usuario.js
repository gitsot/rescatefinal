var mongoose = require('mongoose');
//var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var usuarioSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es obligatorio'] },
    apellido: { type: String, required: [true, 'El apellido es obligatorio'] },
    password: { type: String, required: [true, 'La contrasña es obligatoria'] },
    dni: { type: String, unique: true, required: [true, 'El DNI obligatorio'] },
    id_grupo:{ type:[Schema.Types.ObjectId], ref: 'grupo' },
    esAdmin: { type: Boolean },
});

//pacienteSchema.plugin( uniqueValidator, { message: 'el {PATH} debe ser único' });
module.exports = mongoose.model('usuario', usuarioSchema);