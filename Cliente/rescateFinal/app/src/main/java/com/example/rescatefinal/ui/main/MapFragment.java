package com.example.rescatefinal.ui.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.content.ContextCompat;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.rescatefinal.ClaseGlobal;
import com.example.rescatefinal.R;
import com.example.rescatefinal.busqueda;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class MapFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private GoogleApiClient mGoogleApiClient;
    private Location location;
    private LocationRequest mLocationRequest;
    private GoogleMap mGoogleMap;
    private MapView mMapView;
    private Handler handler = new Handler();
    private Socket socket;
    private HashMap<String, Object> trackUsuarios = new HashMap<String, Object>();
    private JSONObject usu;
    private JSONObject gru;
    private JSONObject bus;
    private String idUsuario;
    private String idBusqueda;
    private String idGrupo;
    String ip = new ClaseGlobal().getIP();
    private boolean appParada = false;
    private int primeraCoor = 0;

    public MapFragment(Socket socket) {
        this.socket = socket;
        socket.connect();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Recojo todos los datos necesarios de usuario, busqueda y grupo
        try {
            usu = ((busqueda) getActivity()).getUsu();
            idUsuario = ((busqueda) getActivity()).getIdUsuario();

            gru = ((busqueda) getActivity()).getGru();
            idGrupo = ((busqueda) getActivity()).getIdGrupo();

            bus = ((busqueda) getActivity()).getBus();
            idBusqueda = ((busqueda) getActivity()).getIdBusqueda();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Obtengo las coordenadas que tengo para coger mi localización
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        FloatingActionButton fab = rootView.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Marcador")
                        .setMessage("Vas a poner un marcador en el mapa para que todos puedan verlo, incluidos otros grupos")
                        .setPositiveButton("Ayuda", new DialogInterface.OnClickListener() { // Necesito ayuda, que venga alguien
                            public void onClick(DialogInterface dialog, int which) {
                                Snackbar.make(view, "Has pedido ayuda, otros buscadores vendrán enseguida.", Snackbar.LENGTH_LONG).show();
                                ponerMarcador("Alguien necesita ayuda");
                            }
                        })
                        .setNegativeButton("Pista", new DialogInterface.OnClickListener() { // Aquí hay una pista
                            public void onClick(DialogInterface dialog, int which) {
                                Snackbar.make(view, "Has marcado en el mapa que has encontrado una pista.", Snackbar.LENGTH_LONG).show();
                                ponerMarcador("Alguien ha encontrado una pista");
                            }
                        })
                        .setNeutralButton("Encontrado", new DialogInterface.OnClickListener() { // Mensaje en chat -> Lo he encontrado!
                            public void onClick(DialogInterface dialog, int which) {
                                Snackbar.make(view, "Has encontrado a quien buscas.", Snackbar.LENGTH_LONG).show();
                                ponerMarcador("Se ha encontrado en este lugar");
                            }
                        })

                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragment
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mGoogleMap = mMap;
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.clear(); //clear old markers
            }
        });
        return rootView;
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public void ponerMarcador(String mensaje) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                .title("Marcador")
                .snippet(mensaje)
                .icon(bitmapDescriptorFromVector(getActivity(), R.drawable.localizador)));

        ArrayList<Double> list = new ArrayList<Double>();
        list.add(location.getLatitude());
        list.add(location.getLongitude());

        JSONObject marcador = new JSONObject();
        JSONArray array = new JSONArray();
        try {
            array.put(Double.toString(location.getLatitude()));
            array.put(Double.toString(location.getLongitude()));

            marcador.put("idUsuario", idUsuario);
            marcador.put("descripcion", mensaje);
            marcador.put("coordenadas", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        socket.emit("marcador", marcador, idBusqueda);

        // Cuando creo un marcador se crea un mensaje destacado para que los participantes de la búsqueda lo puedan ver más rápido
        JSONObject mensajeChat = new JSONObject();
        try {
            mensajeChat.put("mensaje", mensaje);
            mensajeChat.put("idGrupo", idGrupo);
            mensajeChat.put("idUsuario", idUsuario);
            mensajeChat.put("nombre", usu.get("nombre"));
            mensajeChat.put("destacado", true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        socket.emit("mensaje", mensajeChat, idBusqueda);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("TAG", "onConnected");

        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        // Inicio la cámara del mapa
        CameraPosition googlePlex = CameraPosition.builder()
                .target(new LatLng(location.getLatitude(),location.getLongitude()))
                .zoom(18)
                .bearing(0)
                .tilt(45)
                .build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 2500, null);

        pintarZonaAcotada(bus); // Pinto la zona de búsqueda
        pintarZonaGrupo(gru); // Pinto el polígono
        pintarTracksPrevios(idGrupo); // Pinto los tracks anteriores
        pintarMarcadoresPrevios(); // Pinto los marcadores ya guardados en base de datos
        pintaTrackSocket(); // Socket on escuchando los tracks y la posición de todos los usuarios
        pintaMarcadorSocket(); // Pinta los marcadores con los mensajes prefijados de los demás usuarios
        enviaCoordenadas(); // Manda las coordenadas del usuario al servidor para que todos los usuarios puedan recogerlas y guardalas en la base de datos
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("TAG", "onConnectionSuspended");
    }

    private void pintaTrackSocket(){
        /**
         * Este hilo recoge los tracks de los demás dentro del grupo y los pinta en el mapa
         */
        final Map<String, Marker> spy = new HashMap<String, Marker>();
        socket.on(idGrupo, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(args[0].toString());
                            // Pinto el track de todos los usuarios, incluido el mio
                            mGoogleMap.addPolyline(new PolylineOptions()
                                    .add(
                                            new LatLng(json.getDouble("x1"), json.getDouble("y1")),
                                            new LatLng(json.getDouble("x2"), json.getDouble("y2"))
                                    )
                                    .width(5)
                                    .color(Color.RED));

                            // Pinto un marcador con la posición de todos los usuarios menos el mio, porque no se pinta con un marcador, se hace de otra forma
                            if(!json.getString("idUsu").equals(idUsuario)){
                                if(spy.get(json.getString("idUsu"))!=null) { // Si el usuario se está moviendo borra el anterior marcador y pinta el nuevo
                                    Marker mk = spy.get(json.getString("idUsu"));
                                    mk.remove();
                                }

                                if(!json.getBoolean("appParada")) { // Si la aplicación se ha parado para este usuario no tiene que volver a pintar el marcador
                                    spy.put(json.getString("idUsu"), mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(json.getDouble("x1"), json.getDouble("y1")))
                                            .title(json.getString("nombre"))
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))));
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    /**
     * Este hilo escucha los marcadores de todos los demás en la búsqueda
     */
    private void pintaMarcadorSocket(){
        socket.on(idBusqueda+"marc", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONArray jsA = (JSONArray) args[1];
                        try {
                            mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(jsA.getDouble(0), jsA.getDouble(1)))
                                    .title("Marcador")
                                    .snippet(args[0].toString())
                                    .icon(bitmapDescriptorFromVector(getActivity(), R.drawable.localizador)));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    private void enviaCoordenadas(){
        appParada=false;

        /**
         * Creo un hilo para mover el usuario por el mapa y enviar las coordenadas a la base de datos cada segundo
         */
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // Cojo la localización en ese momento
                Location locationn = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                if(primeraCoor>3) {
                    //Actualizo la camara para moverla cuando se mueve el usuario
                    LatLng latLng = new LatLng(locationn.getLatitude(), locationn.getLongitude());
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, mGoogleMap.getCameraPosition().zoom);
                    mGoogleMap.animateCamera(cameraUpdate);
                }else {
                    primeraCoor++;
                }

                // Envío las coordenadas por socket en un json
                JSONObject lacation = new JSONObject();
                JSONObject coors = new JSONObject();
                try {
                    coors.put("x", locationn.getLatitude());
                    coors.put("y", locationn.getLongitude());

                    lacation.put("coors", coors);
                    lacation.put("idUsuario", idUsuario);
                    lacation.put("idGrupo", idGrupo);
                    lacation.put("nombre", usu.get("nombre").toString());
                    lacation.put("appParada", appParada); // Mando esto para que los demás usuarios sepan que se ha parado la aplicación y quitar del mapa el marcador
                    lacation.put("idBusqueda", idBusqueda); 
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Para no guardar siempre la misma localización si la persona no se ha movido compruebo si las coordenadas son las mismas
                if(estaQuieto(locationn.getLatitude(),locationn.getLongitude()) || appParada){
                    socket.emit("location", lacation);
                }

                // Hago que espere cada segundo para volver a mandar localizacion
                if(!appParada) // Si la aplicación no se ha parado vuelve a mandar localizacion
                    handler.postDelayed(this, 1000);
            }
        };
        handler.post(runnable);
    }

    /**
     * Esta función comprueba que el usuario se ha movido para no mandar todas las coordenadas repetidas cada segundo
     * @return
     */
    final float[] xAux = {0};
    final float[] yAux = {0};
    private boolean estaQuieto(Double x, Double y){
        String loc;
        loc = Double.toString(x);
        String[] arrOfStrX = loc.split(".",4);
        while(arrOfStrX[3].length()<7){ // Para que siempre haya 7 digitos
            arrOfStrX[3]+="0";
        }
        int cx = Integer.parseInt(arrOfStrX[3]);

        loc = Double.toString(y);
        String[] arrOfStrY = loc.split(".",4);
        while(arrOfStrY[3].length()<7){ // Para que siempre haya 7 digitos
            arrOfStrY[3]+="0";
        }
        int cy = Integer.parseInt(arrOfStrY[3]);

        if(cx>(xAux[0]+100)||cx<(xAux[0]-100)||cy>(yAux[0]+100)||cy<(yAux[0]-100)){
            xAux[0]=cx;
            yAux[0]=cy;
            return true;
        }else{
            xAux[0]=cx;
            yAux[0]=cy;
            return false;
        }
    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("TAG", "onConnectionFailed");
    }

    @Override
    public void onStop() {
        super.onStop();
        appParada=true; // Dejo de enviar coordenadas
    }

    @Override
    public void onStart() {
        super.onStart();
        if(appParada) // Solo vuelvo a enviar coordenadas si estaba la aplicación parada
            enviaCoordenadas(); // Vuelvo a enviar coordenadas
    }

    /**
     * Pinta los tracks anteriores de todos los usuarios del grupo
     */
    private void pintarTracksPrevios(String grupo){
        String url = "http://"+ip+":3000/coordenadas/"+grupo;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            // Recojo el json de las coordenadas de TODOS los usuarios de un grupo
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray tracks = jsonObject.getJSONArray("json");
                            for (int i = 0; i < tracks.length(); i++) {
                                // Cojo las coordenadas de un usuario
                                JSONObject object = tracks.getJSONObject(i);
                                JSONArray coors = object.getJSONArray("coordenadas");
                                for (int x = 0; x < coors.length()-1; x++) {
                                    // Recojo las coordenadas de dos en dos para pintarlas
                                    JSONObject coor1 = (JSONObject) coors.get(x);
                                    JSONObject coor2 = (JSONObject) coors.get(x+1);

                                    mGoogleMap.addPolyline(new PolylineOptions()
                                            .add(
                                                new LatLng((double)coor1.get("x"), (double)coor1.get("y")), // Cojo la coordenada x
                                                new LatLng((double)coor2.get("x"), (double)coor2.get("y")) // Cojo la coordenada x + 1
                                            )
                                            .width(5)
                                            .color(Color.RED));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        Volley.newRequestQueue( getActivity()).add(postRequest);
    }

    private void pintarZonaAcotada(JSONObject busqueda) {
        try {
            double x = busqueda.getJSONObject("coordenadas").getDouble("x");
            double y = busqueda.getJSONObject("coordenadas").getDouble("y");
            int radio = busqueda.getJSONObject("coordenadas").getInt("radio");

            mGoogleMap.addCircle(new CircleOptions()
                .center(new LatLng(x, y))
                .radius(radio)
                .strokeColor(Color.RED)
                .fillColor(0x30ff0000));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void pintarZonaGrupo(JSONObject grupo) {
        try {
            JSONArray coordenadas = grupo.getJSONArray("coordenadas");

            for(int i = 0; i < coordenadas.length(); i++){
                JSONArray poligono = coordenadas.getJSONArray(i);

                ArrayList<LatLng> point = new ArrayList<LatLng>();
                for(int x = 0; x < poligono.length(); x++){ // Meto los puntos de coordenadas en un array para después pintar el poligono
                    point.add(new LatLng(poligono.getJSONObject(x).getDouble("x"), poligono.getJSONObject(x).getDouble("y")));
                }

                // creo poligono
                LatLng[] points = point.toArray(new LatLng[point.size()]);
                mGoogleMap.addPolygon(new PolygonOptions()
                .fillColor(0x30fff000)
                .add(points));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void pintarMarcadoresPrevios(){
        try {
            JSONArray marcadores = (JSONArray) bus.get("marcadores");
            for(int i=0; i<marcadores.length(); i++){
                JSONObject marcador = (JSONObject) marcadores.get(i);
                JSONArray coors = (JSONArray) marcador.get("coordenadas");

                mGoogleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(coors.getDouble(0), coors.getDouble(1)))
                        .title("Marcador")
                        .snippet(marcador.getString("descripcion"))
                        .icon(bitmapDescriptorFromVector(getActivity(), R.drawable.localizador)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}