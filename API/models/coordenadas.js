var mongoose = require('mongoose');
//var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var coordenadasSchema = new Schema({
    coordenadas: { type: [JSON], required: [true, 'Las coordenadas son obligatorias'] },
    id_Usuario:{ type:Schema.Types.ObjectId, ref: 'usuario', required: [true, 'El usuario es obligatorio'] },
    id_Grupo:{ type:Schema.Types.ObjectId, ref: 'grupo', required: [true, 'El grupo es obligatorio'] },
});

//pacienteSchema.plugin( uniqueValidator, { message: 'el {PATH} debe ser único' });
module.exports = mongoose.model('coordenadas', coordenadasSchema);