// Requires
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

// Inicializar variables
var api = express();
// Variables del servidor de sockets
var server = require('http').Server(api);
var io = require('socket.io')(server);

// CORS
var cors = require('cors');
api.use(cors());
api.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/**
 * Importar rutas
 */
var usuarioRoutes = require('./routes/usuario');
var busquedaRoutes = require('./routes/busqueda');
var grupoRoutes = require('./routes/grupo');
var loginRoutes = require('./routes/login');
var coordenadaRoute = require('./routes/coordenadas');
var mensajeRoute = require('./routes/mensajes');

// Conexión a la base de datos
mongoose.connection.openUri('mongodb://localhost:27017/rescateFinal', (err, res) => {
  if (err) throw err;
});


/**
 * Body Parser
 */
// parse application/x-www-form-urlencoded
api.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
api.use(bodyParser.json());

// Para ver las imagenes por la url
api.use(express.static(__dirname));


// Rutas
api.use('/usuario', usuarioRoutes);
api.use('/busqueda', busquedaRoutes);
api.use('/grupo', grupoRoutes);
api.use('/login', loginRoutes);
api.use('/coordenadas', coordenadaRoute);
api.use('/mensajes', mensajeRoute);

// Escuchar peticiones
var port = 3000;
api.listen(port, () => {
  console.log("Express funcionando en el puerto "+port);
});

/**
 * Inicio el servidor de sockets en el puerto 4000
 */
var socketController = require('./controllers/socketController');
io.on('connection', function (socket){
  console.log("alguien se ha conectado");

  // Recojo la localización
  socket.on('location', function(coors){

    socketController.localizacion(coors); // Guardo localización en base de datos
    var coorTrack = socketController.trackBroadcast(coors); // Envío coordenadas a todos los usuarios de ese grupo para pintar el track en el mapa
    socket.broadcast.emit(coors.idGrupo, coorTrack); // Con esto estoy mandando las coordenadas a TODOS los del grupo
    socket.emit(coors.idGrupo, coorTrack);
    socket.broadcast.emit(coors.idBusqueda, coorTrack);
  });

  socket.on('mensaje', function(msg, idBusqueda){
    if(msg.destacado!=undefined){ // Si el mensaje es destacado se manda a TODOS los usuarios de esa búsqueda
      socketController.EnviarDestacadoABusqueda(idBusqueda, msg, socket, hora());
    }else{
      socketController.chat(msg); // Guardo el mensaje en la base de datos
      socket.broadcast.emit(msg.idGrupo+"msg", msg.mensaje, msg.idUsuario, hora(), msg.nombre, false); // Con esto estoy mandando los mensajes a TODOS los del grupo
      socket.emit(msg.idGrupo+"msg", msg.mensaje, msg.idUsuario, hora(), msg.nombre, false); // y me lo mando a mi mismo
    }
  });

  socket.on('notificacionInterna', function(noti){
    console.log(noti)
    socket.broadcast.emit("notificacion", "hola");
  });

  socket.on('marcador', function(marc, idBusqueda){
    socketController.marcador(marc, idBusqueda);
    socket.broadcast.emit(idBusqueda+"marc", marc.descripcion, marc.coordenadas); // Con esto estoy mandando los marcadores a todos los del grupo
  });
});

// Obtiene la hora de envío del mensaje
function hora(){
  var date = new Date();
  var localeSpecificTime = date.toLocaleTimeString();
  return localeSpecificTime.replace(/:\d+ /, ' ');
}

server.listen(4000, function () {
  console.log("Servidor sockets corriendo en el puerto 4000")
});