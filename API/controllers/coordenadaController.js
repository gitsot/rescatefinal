var Coordenada = require('../models/coordenadas');

function getTracks(req, res, next){
    Coordenada.find({ id_Grupo: req.params.idGrupo })
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    json
                });
            });
}

function getTracksUsuarioGrupo(req, res, next){
    Coordenada.findOne({$and: [{"id_Usuario" : req.params.idUsuario}, {"id_Grupo" : req.params.idGrupo}] })
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    json
                });
            });
}
function deleteCoordenadas(req, res, next){
    var id = req.params.id;

    Coordenada.findByIdAndRemove(id, (err, CoordenadaBorrada)=>{
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar las coordenadas',
                errors: err
            });
        }

        if (!CoordenadaBorrada){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe esos mensajes con ese id',
            });
        }

        /*res.status(200).json({
            ok: true,
            consulta: GrupoBorrado
        });*/
    });
}

module.exports = {
    getTracks,
    getTracksUsuarioGrupo,
    deleteCoordenadas
};