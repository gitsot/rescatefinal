var Grupo = require('../models/grupo');
var Usuario = require('../models/usuario');
var Mensajes = require('../models/mensajes');
var Coordenadas = require('../models/coordenadas');

function borrarRelacionGrupo(idGrupo, idUsuario){
    // Borro la relacion en el grupo
    Grupo.findById(idGrupo, (err, grupo)=>{
        /*if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar el grupo',
                errors: err
            });
        }

        if (!grupo){
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Ese grupo no existe',
                    errors: err
                });
            }
        }*/

        if(grupo.id_usuario.includes(idUsuario)){ // Solo lo borra si lo contiene
            grupo.id_usuario.pull(idUsuario);
        }

        grupo.save( ( err, grupoGuardado ) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar el grupo',
                    errors: err
                });
            }

            /*res.status(200).json({
                ok: true,
                paciente: busquedaGuardada
            });*/
        });
    });
}

function borrarRelacionUsuario(idUsuario, idGrupo) {
// Borro la relacion en el usuario
    Usuario.findById(idUsuario, (err, usuario)=>{
        /*if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar el usuario',
                errors: err
            });
        }

        if (!usuario){
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Ese usuario no existe',
                    errors: err
                });
            }
        }*/

        if(usuario.id_grupo.includes(idGrupo)){ // Solo lo borra si lo contiene
            usuario.id_grupo.pull(idGrupo);
        }

        usuario.save( ( err, usuarioGuardado ) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar el usuario',
                    errors: err
                });
            }

            /*res.status(200).json({
                ok: true,
                paciente: usuarioGuardado
            });*/
        });
    });
}

/**
 * Borra los mensajes de un usuario en el array del grupo
 * @param idUsuario
 * @param idGrupo
 */
function borrarRelacionMensajes(idGrupo, idUsuario) {
    Mensajes.findOne({ id_grupo : idGrupo })
        .exec(
            (err, json) => {
                if (err){
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error de base de datos',
                        errors: err
                    });
                }

                json.mensaje.forEach( function(valor, indice, array) {
                    if(valor.id_usuario.includes(idUsuario)) { // Solo lo borra si lo contiene
                        json.mensaje.pull(valor);
                    }
                });

                json.save( ( err, usuarioGuardado ) => {
                    if (err){
                        return res.status(400).json({
                            ok: false,
                            mensaje: 'Error al actualizar el usuario',
                            errors: err
                        });
                    }

                /*res.status(200).json({
                    ok: true,
                    json
                });*/
            });
    });
}

/**
 * Borra las coordenadas de un usuario en el array de coordenadas
 * @param idUsuario
 * @param idGrupo
 */
function borrarRelacionCoordenadas(idUsuario) {
    Coordenadas.remove({ id_Usuario : idUsuario }, function(err) {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
    });
}


module.exports = {
    borrarRelacionGrupo,
    borrarRelacionUsuario,
    borrarRelacionMensajes,
    borrarRelacionCoordenadas
};